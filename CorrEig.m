% function to try to find "ideal" forms for C1 and C2
% 
% function [C1, C2, u1, u2] = CorrEig(sE1, sI, sE2);
% load('ConDefinitions.mat')

d = 2;%topographic/retinotopy dimension 
L = 10
if d == 1
    nDim = 1024; %previous: 512, 256,  NOTE 256 = 16^2
elseif d == 2
    nDim = 32; % previous 16
    L = 16
end

spacing = 2*L/(nDim);
PBC = 1;%1 = impose periodic B.C.'s

nV1 = nDim^d; %number of V1 neurons -- one dimensional currently
nThal = nV1; %sets size of input layer. =nV1 means equal number of input and output neurons

eps = 0;%0.1; %sets scale of epsilon. Set eps=0 to not include order epsilon terms in dW/dt

aE = 10;
aI = 10;

sigE = 0.5;
sigI = 3*sigE;
sigf = 0.01; %spacing/5;%0.01;
sE1 = 0.3; % defines the correlations sigmas
sI = 1;
sE2 = 0.1/3; % got this from Analyzing things in CorrEig;
sI2 = 0.3/3;

tau1 = 100;
tau2 = 1;
tauW = 1000;
dt = 0.1*tau2; % dt sets the time scale -- currently 1/10 of tau2.

gs = 1/tau1; %this is gamma_s from Yashar's notes -- arbitrarily setting gamma_s = 1/tau1
gf = 1/tau2; % this is gamma_f from Yashar's notes

[distsq, X] = neurDistance2(nDim, d, L,PBC);
distV1 = sqrt(distsq);
%)))))))))))))))))))))))))))))))))))))))

if d == 1
    D = 1;
elseif d == 2
    D = 5*spacing;
end

% sE = sigE;
% sI = sigI;
% sigE = [0.1:0.1:2]*sigE;
% sigI = [0.1:0.1:2]*sigI;

c_a = 0.5;
[A,~,~,RFinds,Nx_arbor] = Arbor2(sqrt(distsq), D,d, nDim,nDim,L, c_a);

% M = sum(A(1,:)); %finds the number of neurons in the arbor, for 1D

% x1 = linspace(-D, D, M);
% x1 = repmat(x1, M,1);
% x2 = x1';
% distsq = (x1 - x2).^2;
% [distsq, X] = neurDistance2(M, d, D,PBC);
%}
M = nDim;
xx = linspace(-D, D, M);
xx = -D:spacing:D;
Xs = repmat(xx',1,length(xx));%repeated columns
distsq = abs(Xs - Xs');
M = length(xx);

% [C1, C2, e1,e2,~] = CorrProj(X,nDim, 'corr1', );
% [p1, p2, a1,a2,~] = CorrProj(dist,nThal, 'corr2', varargin);
sE1 = [0.1/3,0.1,0.3,1,3,10];
sI = [0.1, 0.3, 0.75, 1, 3, 10];
sE2 = sE1;

s1 = zeros(1,length(sE1), length(sI));
% ds1 = s1;
s2 = zeros(2,length(sE2));
% dk2 = s1;
% vk1 = zeros(nDim, length(sigE), length(sigI));
% vk2 = vk1;
% L1 = s1;
% L2 = s1;
u1 = zeros(M, length(sE1), length(sI));
u2 = zeros(M, length(sE2));
% dL1 = s1;
% dL2 = s2;
p1_pars.aEI = [aE, aI];
p2_pars.aEI = [aE, aI];

for ee = 1:length(sE1)
    for ii = 1:length(sI)
        if sI(ii) > sE1(ee)
            %uncomment this out if I'm concerned about extended e1's
            C_e = exp(-distsq/(2*sE1(ee).^2));
            C_e = C_e./(sum(C_e(1,:)));
            C_i = exp(-distsq/(2*sI(ii).^2));
            C_i = C_i./(sum(C_i(1,:)));
            C1 = aE*C_e - aI*C_i;
%             C1 = A.*C1;
           [u, s,v] = svds(C1, 1);
            s(s==0) = []; 
            if and(s~=1, ~isnan(s))
                C1 = (1/s)*C1;
                [u, s, ~] = svds(C1,1);
            end
            
            
%             %for truncated eigenvectors
%             p1_pars.sigEI = [sE1(ee), sI(ii)];
%             p2_pars.sigEI = [sE1(ee), sI(ii)];
%             [u, ~,~, s, ~] = RFcovPCs(spacing,d, p1_pars,p2_pars, D);

            u1(:,ee, ii) = u(:,1);
            s1(:,ee, ii) = s;
            
            
        end
    end
end

u11 = reshape(u1, M, length(sE1)*length(sI));

% for e2 = 1:length(sE2)
%     C2 = exp(-distsq/(2*sE2(e2).^2));
%     C2 = C2./sum(C2(1,:));
% 
%     [u, s,v] = svds(C2, 2);
%     s(s==0) = [];
%     u2(:, e2) = u(:,1);
%     s2(:, e2) = s;
% end

                
%                 
%                 %find kernels for A1, and A2
%                 [Cs, Cf, ~,~,~] = CorrProj(X,nDim, 'corr2', sigE(ee), sigI(ii), sE, sI, aE, aI, D);
%                 %             [p1, p2, ~,~,~] = CorrProj(dist,nThal, 'corr2', sigE(ee), sigI(ii), sE, sI, aE, aI, D);
%                 
%                 [e1, vv1] = eig(Cs, 'vector');
%                 [e2, vv2] = eig(Cf, 'vector');
%                 [L1(ee,ii), ind1] = max(vv1);
%                 [L2(ee, ii), ind2] = max(vv1);
%                 v1(:, ee, ii) = e1(:, ind1);
%                 v2(:, ee, ii) = e2(:, ind2);
%                 
%                 if ~or(ind1 == 1, ind2 ==1)
%                     dL1(ee, ii) = L1(ee,ii) - vv1(ind1-1);
%                     dL2(ee, ii) = L2(ee, ii) - vv2(ind2-1);
%                 elseif ind1 == 1
%                     dL1(ee, ii) = L1(ee,ii) - vv1(ind1+1);
%                     dL2(ee, ii) = L2(ee, ii) - vv2(ind2-1);
%                 elseif ind2 == 1
%                     dL1(ee, ii) = L1(ee,ii) - vv1(ind1-1);
%                     dL2(ee, ii) = L2(ee, ii) - vv2(ind2+1);
%                 end
%                 
%                 vk1(:,ee, ii) = fft(Cs(1,:));
%                 vk2(:,ee, ii) = fft(Cf(1,:));
%                 [s1(ee, ii), ind1] = max(real(vk1(:,ee ,ii)));
%                 [s2(ee, ii), ind2] = max(real(vk2(:,ee,ii)));
%                 if ~or(ind1 == 1, ind2 ==1)
%                     ds1(ee, ii) = s1(ee,ii) - vk1(ind1-1, ee, ii);
%                     dk2(ee, ii) = s2(ee,ii) - vk2(ind2-1, ee, ii);
%                 elseif ind1 == 1
%                     ds1(ee, ii) = s1(ee, ii) - vk1(ind1+1, ee, ii);
%                     dk2(ee, ii) = s2(ee,ii) - vk2(ind2-1, ee, ii);
%                 elseif ind2 == 1
%                     ds1(ee, ii) = s1(ee,ii) - vk1(ind1-1, ee, ii);
%                     dk2(ee, ii) = s2(ee,ii) - vk2(ind2+1, ee, ii);
%                 end
% 
% end
%     end
% end
%{
if length(sigI) == 1
    figure(1);
    plot(sigE, L1, sigE, s1);
    title('Eigenvalues or k-values of Cs')
    xlabel('\sigma_E')
    ylabel('$\widetilde{\lambda_1}$ or $k_1$', 'Interpreter', 'latex')
    legend('Eig', 'FFT')
    
    figure(2);
    plot(sigE, L2, sigE, s2);
    title('Eigenvalues or k-values of Cf')
    xlabel('\sigma_E')
    ylabel('$\widetilde{\lambda_2}$ or $k_2$', 'Interpreter', 'latex')
    legend('Eig', 'FFT')
    
    figure(3);
    plot(sigE, L1./L2, sigE, s1./s2,'LineWidth', 1.25);
    title('Ratio of Eigenvalues or k-values (fixed \sigma_I)')
    xlabel('\sigma_E')
    ylabel('$\frac{\widetilde{\lambda_1}}{\widetilde{\lambda_2}}$ or $\frac{k_1}{k_2}$', 'Interpreter', 'latex')
    legend('Eig', 'FFT')
    
%     exportpdf(3, 'EigenRatio_sigIFixed', [3.5 0.7])
    
elseif length(sigE) == 1
    figure(1);
    plot(sigE, L1, sigE, s1);
    title('Eigenvalues or k-values of Cs')
    xlabel('\sigma_I')
    ylabel('$\widetilde{\lambda_1}$ or $k_1$', 'Interpreter', 'latex')
    legend('Eig', 'FFT')
    
    figure(2);
    plot(sigE, L2, sigE, s2);
    title('Eigenvalues or k-values of A2')
    xlabel('\sigma_I')
    ylabel('$\widetilde{\lambda_1}$ or $k_1$', 'Interpreter', 'latex')
    legend('Eig', 'FFT')
    
    figure(3);
    plot(sigE, L1./L2, sigE, s1./s2,'LineWidth', 1.25);
    title('Ratio of Eigenvalues or k-values (fixed \sigma_E)')
    xlabel('\sigma_I')
    ylabel('$\frac{\widetilde{\lambda_1}}{\widetilde{\lambda_2}}$ or $\frac{k_1}{k_2}$', 'Interpreter', 'latex')
    legend('Eig', 'FFT')
    
%     exportpdf(3, 'EigenRatio_sigEFixed', [3.5 0.7])
    
else    
    figure;
    subplot(1,2,1)
    imagesc(sigE, sigI, s1);
    title('FFT solution of k-values Cs')
    xlabel('\sigma_E')
    ylabel('\sigma_I')
    colorbar
    
    subplot(1,2,2)
    imagesc(sigE, sigI, L1);
    title('Numeric Solutions of eigenvalues for Cs')
    xlabel('\sigma_E')
    ylabel('\sigma_I')
    colorbar
    
    figure;
    subplot(1,2,1)
    imagesc(sigE, sigI, s2);
    title('FFT solution of k-values Cf')
    xlabel('\sigma_E')
    ylabel('\sigma_I')
    colorbar
    
    subplot(1,2,2)
    imagesc(sigE, sigI, L2);
    title('Numeric Solutions of eigenvalues Cf')
    xlabel('\sigma_E')
    ylabel('\sigma_I')
    colorbar
    
    figure;
    subplot(1,2,1)
    imagesc(sigE, sigI, s1./s2);
    title('FFT solution of eigenvalues (k-values) Ratio')
    xlabel('\sigma_E')
    ylabel('\sigma_I')
    colorbar
    
    subplot(1,2,2)
    imagesc(sigE, sigI, L1./L2);
    title('Numeric Solutions of eigenvalues Ratio')
    xlabel('\sigma_E')
    ylabel('\sigma_I')
    colorbar
end
%}