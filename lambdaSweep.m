% this will run visualMaps for many realizations and find the variance and
% nearest neighbor jumps for many values of lambda2. And also for various
% alpha_f's (af).

lambda2 = [0.1, 0.3, 1, 3, 10, 30, 100, 300];
af = [0.1, 0.3, 1, 3, 10, 30, 100, 300]*1.5038; %1.5038 is the size of aE with the given number of nuerons and L
runs = 5; %number of runs I want to average over -- also so I can reject ones that didn't work due to IC
nDim = 400;
L = 10;

laf = length(af);
mods = 2;%laf^2;

maps = zeros(nDim, runs, mods);
salt = maps;
neither = maps;
% 
% m.q1 = zeros(runs, mods);
% m.q2 = m.q1;
% m.q3 = m.q1;
% 
% s.q1 = m.q1;
% s.q2  = m.q1;
% s.q3 = m.q1;
% 
% n.q1 = m.q1;
% n.q2 = m.q1;
% n.q3 = m.q1;

q1m = zeros(runs, mods);
q1s = q1m;
q1n = q1m;

q2m = q1m;
q2s = q1m;
q2n = q1m;

q3m = q1m;
q3s = q1m;
q3n = q1m;

rAf = zeros(mods,1);
rL2 = zeros(mods,1);

parfor ii = 1:mods
    
    [il2, iaf] = ind2sub([laf, laf], ii);
    rAf(ii) = af(iaf);
    rL2(ii) = lambda2(il2);
    
    for rr = 1:runs
        [rMaps, rSalt, rNeither, rM, rS, rN] = visualMaps(lambda2(il2), af(iaf), nDim, L)
        maps(:, rr, ii) = rMaps;
        salt(:, rr, ii) = rSalt;
        neither(:, rr, ii) = rNeither;
        
        q1m(rr, ii) = rM.q1;
        q2m(rr, ii) = rM.q2;
        q3m(rr,ii) = rM.q3;
        
        q1s(rr,ii) = rS.q1;
        q2s(rr,ii) = rS.q2;
        q3s(rr,ii) = rS.q3;
        
        q1n(rr,ii) = rN.q1;
        q2n(rr,ii) = rN.q2;
        q3n(rr,ii) = rN.q3;
    end
end

save('Lambda2Sweep','-v7.3')

