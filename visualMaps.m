% Simulate a network of neurons and see how 'maps' develop using complex
% synapses
%Takes tau1 as its input function. Tau2 is set to 1. TauW = 100. All these
%are in indeterminate units of 'time steps'. 
function [m, s, n, lTrue, growth,wp, W, Inp] = visualMaps(lambda2, af, varargin)  %visualMaps(tau1, nDim) 

% the third element of both lTrue and growth is the ratio of lTrue1/lTrue2,
% similar for growth.

if nargin > 2
    ii = varargin{1};
end

load('ConDefinitions.mat'); % file that holds constants values. to be called in multiple functions
Inp.sigE = sigE;
Inp.sigI = sigI;
Inp.aE = aE;
Inp.aI = aI;
Inp.spacing = spacing;
Inp.PBC = PBC; %determines if we're using Periodic Boundary conditions (PBC = 1) or not (PBC = 0)
Inp.distV1 = distV1;

SPARSE = 0; %determines if we use W as a sparse (SPARSE = 1) or not (SPARSE = 0) matrix
SIMPLE = 0; % 1 - step fcn approx, 2 - integral form, 3 - analytic solution
NORM = 1; %Doesn't normalize W (Norm=0), or uses Miller norm (NORM = 1);
ARBOR = 1;

Inp.Norm = NORM;
eps = 0.1; %sets scale of epsilon. Set eps=0 to not include order epsilon terms in dW/dt
Inp.eps = eps;

nV1 = nDim^d; %number of V1 neurons -- one dimensional currently
Inp.nV1 = nV1;
dt = 0.1*tau2; % dt sets the time scale -- currently 1/10 of tau2.

nThal = nV1; %sets size of input layer. =nV1 means equal number of input and output neurons
Inp.nThal = nThal;%floor(nV1/2);

% %make projection operators by Graham-Schmidt Orthonormalization
% e1 = randn(nThal, 1);
% e1 = e1/sqrt(e1'*e1);
% p1 = e1*e1';
% e2 = randn(nThal, 1);
% e2 = e2 - (e1'*e2)*e1;
% e2 = e2/sqrt(e2'*e2);
% p2 = e2*e2';
% e3 = randn(nThal,1);
% e3 = e3 - (e2'*e3)*e2 - (e1'*e3)*e1;
% e3 = e3/sqrt(e3'*e3);
[p1, p2, e1, e2, e3] = CorrProj(distV1, nThal, 'corr1', sigE, sigI, 0.5*sigE, 0.5*sigI);%3,5,spacing/5, spacing

Inp.e1 = e1;
Inp.e2 = e2;
Inp.e3 = e3;

lambda1 = 1; %lambda2 is given as first input to function.
Inp.lambda1 = lambda1; 
Inp.tau = [tau1; tau2; tauW]; %tau1, tau2, tauW defined in ConDefinitions.mat
Inp.scaleTau = tauW/(lambda1*aE);
if NORM
    ts = 5*10*tauW/(lambda1*aE); % to run for long enough time so that dynamics are well-developped.
    Inp.totalTime = ts;
    
    nsteps = round(ts/dt);
    Inp.Nsteps = nsteps;
    
    wp1 = zeros(nV1, (nsteps/10)+1); % I don't save every times step so wp's stay smaller in size
    wp2 = zeros(nV1, (nsteps/10)+1);
    wp3 = wp1;
    wp = zeros(nV1, (nsteps/10)+1, 3);
else
    ts = 5*10*tauW/(lambda1*aE); % to run for long enough time so that dynamics are well-developped.
    Inp.totalTime = ts;
    
    nsteps = round(ts/dt);
    Inp.Nsteps = nsteps;
    
    wp1 = zeros(nV1, (nsteps/10)+1); % I don't save every times step so wp's stay smaller in size
    wp2 = zeros(nV1, (nsteps/10)+1);
    wp3 = wp1;
    wp = zeros(nV1, (nsteps/10)+1, 3);
end

%define G's 
G_E = exp(-distV1./(2*(sigE).^2));
G_E = G_E/sum(G_E(1,:)); %normalizes G_E
G_I = exp(-distV1./(2*(sigI).^2));
G_I = G_I/sum(G_I(1,:)); %normalizes G_I
Gs = aE*G_E - aI*G_I; %combines the two into a Mexican Hat
Gf = exp(-distV1./(2*(sigf).^2));
Gf = Gf/sum(Gf(1,:)); %%normalizes Gf
Gf = af*Gf;

Inp.Gs = Gs;
Inp.Gf = Gf;

[V1, eigA1] = eig((lambda1/4)*(0.5*Gs + 1/(1+ (tau2/tau1))*Gf), 'vector');
[lTrue1, ind1] = max(real(eigA1));

[V2, eigA2] = eig((lambda2/4)*(1/(1+(tau1/tau2))*Gs + 0.5*Gf), 'vector');
[lTrue2, ind2] = max(real(eigA2));
lTrue = [lTrue1; lTrue2; lTrue1/lTrue2];

gs = 1/tau1; %this is gamma_s from Yashar's notes -- arbitrarily setting gamma_s = 1/tau1
gf = 1/tau2; % this is gamma_f from Yashar's notes

if ARBOR 
    D = 2;
    A = Arbor(X, D);
elseif ~ARBOR
    A = ones(size(W));
end
Inp.A = A;
if SIMPLE ==1 % approximation using step functions
    dWdt = @(W) lambda1*(Gs + Gf)*(W*p1) + lambda2*((tau2/tau1)*Gs + Gf)*W*p2;
elseif SIMPLE == 2 % this is the integral form 
    KC = @(f, W) (Gs.*(abs(1 + 2*pi*1i*f/gs)).^(-2) + Gf.*(abs(1 + 2*pi*1i*f/gf)).^(-2))*W*...
        (lambda1*tau1*(abs(1 + 2*pi*1i*f*tau1).^(-2)).*p1 + lambda2*tau2*(abs(1 + 2*pi*1i*f*tau2).^(-2)).*p2);
    %KC multiplies the real part of K with C(f) for ease of use with integral 
    dWdt = @(W) integral(@(f) KC(f,W), 0, inf, 'ArrayValued', true);
elseif SIMPLE == 0 % analytic solution to the integral I derived. 
    dWdt = @(W) A.*((lambda1/4)*(0.5*Gs + 1/(1+ (tau2/tau1))*Gf)*W*p1 + (lambda2/4)*(1/(1+(tau1/tau2))*Gs + 0.5*Gf)*W*p2+(eps/4)*(gs*Gs + gf*Gf)*W); %this adds a more complete term. ;     
end

Wmax = 4;
if ~SPARSE
    W = (Wmax/4)*rand(nV1, nThal);%(Wmax/4) is so we don't start close to the upper bound
    W = A.*W;
    if NORM
        W = repmat(sum(A,2),1, nThal).*W./repmat(sum(W,2),1,nThal); % nThal*W./repmat(sum(W,2),1,nThal);
        Wact = zeros(nV1,1);
        Wfroz = Wact;
    end
    
elseif SPARSE
    sparsity = 0.1;
    W = rand(nV1, nThal);
    W(W > sparsity) = 0;
    W(W ~= 0) = Wmax*rand;
    nIn = sum(W ~=0 ,2);
    W = nIn.*W./repmat(sum(W,2),1,nThal);
end
% logicW = or(W >= Wmax, W <= 0);
% dW = zeros(nV1, nThal); % apparently doesn't need to be pre-allocated 
% wtracker = zeros([size(W), nsteps+1]);
% wtracker(:,:, 1) = W;
% wp1 = zeros(nV1, (nsteps/1000)+1); % I don't save every times step so wp's stay smaller in size
% wp2 = zeros(nV1, (nsteps/1000)+1);
% wp3 = wp1;
% % q1 = zeros(3, (nsteps/10)+1); % also true with q's
% wp = zeros(nV1, (nsteps/1000)+1, 3);
% wp1 = zeros(nV1, nsteps+1);
% wp2 = zeros(nV1, nsteps+1);
% wp3 = wp1;
% q1 = zeros(3, nsteps+1);

wp1(:,1) = W*e1;
wp2(:,1) = W*e2;
wp3(:,1) = W*e3;

% growth1 = exp(lTrue1)*(V1(:,ind1)'*wp1(:,1));
growth1 = (V1(:,ind1)'*wp1(:,1));
% growth2 = exp(lTrue2)*(V2(:,ind2)'*wp2(:,1));
growth2 = (V2(:,ind2)'*wp2(:,1));
growth = [growth1; growth2; growth1/growth2];

% q1(1,1) = sum(W*e1).^2;
% q1(2,1) = sum(W*e2).^2;
% q1(3,1) = sum(W*e3).^2;

if NORM
%     while sum(sum(or(W >= Wmax, W <= 0),2)) ~= nV1*nThal % could do it
%     this way to make all synapses inactive or
    for t =1:nsteps
        dW = (1/tauW)*dWdt(W);
        
        dW(W >= Wmax) = 0;
        dW(W <= 0) = 0;
        nAct = sum(and(W < Wmax, W >0),2);
        if sum(nAct) == 0
            Inp.endT = t;
            break
        end 
        epsilon = sum(dW,2)./nAct; %epsilon will constrain the growth of W. 
        epsilon = repmat(epsilon, 1, nThal);
        epsilon(or(W >= Wmax, W <=0)) = 0;
        epsilon = A.*epsilon; %from eq 2- Miller 1994 paper. It's the arbor function times the contraining epsilon. 
        dW = dW - epsilon;
        W1 = dW*dt + W;
        W1(W1 > Wmax) = Wmax;
        W1(W1 < 0) = 0;
        
        Wact = sum(W1.*and(W1 < Wmax, W1 > 0),2);
        Wfroz = sum(W1.*or(W1 >=Wmax, W1<= 0),2);
        
        
%         for cc = 1:nV1
%             Wact(cc) = sum(W1(cc, dW(cc,:)~=0));
%             Wfroz(cc) = sum(W1(cc, dW(cc,:)==0));
%         end
       
        gamma = (-Wfroz + nThal)./Wact;
        gamma = repmat(gamma, 1, nThal);
        
        
        gamma(or(W1 >= Wmax, W1<=0)) = 1;
        W = W1.*gamma;
        
        if mod(t,10) == 0
            %      wtracker(:,:,t+1) = W;
            wp1(:,(t/10)+1) = W*e1;
            wp2(:,(t/10)+1) = W*e2;
            wp3(:,(t/10)+1) = W*e3;
            
%             q1(1,(t/10)+1) = sum(W*e1).^2;
%             q1(2,(t/10)+1) = sum(W*e2).^2;
%             q1(3,(t/10)+1) = sum(W*e3).^2;
        end
    end
    
elseif ~NORM
    for t = 1:nsteps
        dW = (1/tauW)*dWdt(W);
        
        W = dW*dt + W;
        
        if mod(t,10) == 0
            %      wtracker(:,:,t+1) = W;
            wp1(:,(t/10)+1) = W*e1;
            wp2(:,(t/10)+1) = W*e2;
            wp3(:,(t/10)+1) = W*e3;
            
%             q1(1,(t/10)+1) = sum(W*e1).^2;
%             q1(2,(t/10)+1) = sum(W*e2).^2;
%             q1(3,(t/10)+1) = sum(W*e3).^2; 
        end
    end
end

Inp.endT = t;
maps = W*e1;%W*v(sap:,ind(end));
salt =  W*e2;%W*v(:,ind(end-1));
neither = W*e3;
Inp.x = -L +(0:(nV1-1))*spacing;

m.Q = [mean(wp1.^2); mean(diff(wp1).^2)./mean(wp1.^2) ; mean(wp1)]; % the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
s.Q = [mean(wp2.^2); mean(diff(wp2).^2)./mean(wp2.^2); mean(wp2)];
n.Q = [mean(wp3.^2); mean(diff(wp3).^2)./mean(wp3.^2); mean(wp3)];

wp(:,:,1) = wp1;
wp(:,:,2) = wp2;
wp(:,:,3) = wp3;

% H = MapsPlots(m, s, n, wp, growth, Inp,3);

if nargin > 2
    figname = num2str(ii);
    if ARBOR ~= 0
        figname = strcat('A', figname);
    end
    if NORM ~= 0
        figname = strcat('Norm', figname);
    end
    if eps ~=0
        figname = strcat('Eps',figname);
    end
    
    figname = strcat('EqLambda',figname);
    
%     savefig(H, figname)
    save(figname, 'm', 's', 'n', 'growth', 'lTrue', 'wp', 'Inp','W', '-v7.3')
end 


%from here on down it's just random comments I didn't need. 
%{
% H(1) = figure;
% % x = 1:length(maps);
% subplot(2,3,1)
% plot(x, maps);
% title('Projection of W on slow vector')
% xlabel('Neuron Position')
% ylabel('Proj(W)')
% 
% % figure(2)
% subplot(2,3,2)
% % x = 1:length(salt);
% plot(x, salt)
% title('Projection of W on fast vector')
% xlabel('Neuron Position')
% ylabel('Proj(W)')
% 
% % figure(3)
% % x = 1:length(neither);
% subplot(2,3,3)
% plot(x, neither)
% title('Projection of W on Neither')
% xlabel('Neuron Position')
% ylabel('Proj(W)')
% 
% dk = 1/spacing; %spacing is the spatial period, dk = spatial frequency
% k = (0:dk:(nDim))/nDim;
% fmaps = fft(maps);
% fsalt = fft(salt);
% fneither = fft(neither);
% 
% % figure(4)
% subplot(2,3,4)
% plot(k, fmaps(1:length(k)))
% title('FFT of ProjW on slow')
% xlabel('Spatial freq')
% ylabel('Proj(W)')
% 
% % figure(5)
% subplot(2,3,5)
% plot(k, fsalt(1:length(k)))
% title('FFT of ProjW on fast')
% xlabel('Spatial freq')
% ylabel('Proj(W)')
% 
% % figure(6)
% subplot(2,3,6)
% plot(k, fneither(1:length(k)))
% title('FFT of ProjW on neither')
% xlabel('Spatial freq')
% ylabel('Proj(W)')


% H(2) = figure;
% plot(x,(lambda2/4)*(1/(1+(tau1/tau2))*Gs(200,:) + 0.5*Gf(200,:)))
% title('Fast vector Connection Profile')
% 
% H(3) = figure;
% plot(x, (lambda1/4)*(0.5*Gs(200,:) + 1/(1+ (tau2/tau1))*Gf(200,:)))
% title('Slow vector Connection Profile')
% 
% H(4) = figure;
% % mesh(x', 0:dt:ts, wp1')
% mesh(x', 0:dt*10:ts, wp1')
% title('Projection on Slow Vector')
% 
% H(5) = figure;
% % mesh(x', 0:dt:ts, wp2')
% mesh(x', 0:dt*10:ts, wp2')
% title('Projection on fast Vector')
% 
% H(6) = figure;
% % mesh(x', 0:dt:ts, wp3')
% mesh(x', 0:dt*10:ts, wp3')
% title('Projection on neither Vector')
% 
% H(7) = figure;
% plot(x', wp2(:,1), x', wp2(:,end))
% title('Proj on fast, beginning and end')
% legend('t = 1', 't = end')
% 
% H(8) = figure;
% plot(x', sign(wp2(:,end).*wp2(:,1)).*abs((wp2(:,end)-wp2(:,1))./nsteps))
% title('abs(slope of proj on fast)')
% 
% H(9) =figure;
% plot(x', sign(wp3(:,end).*wp3(:,1)).*abs((wp3(:,end)-wp3(:,1))./nsteps))
% title('(slope of proj on neither)')
% 
% H(10) = figure;
% subplot(1,3,1)
% % plot(0:dt:ts, q1(1,:))
% plot(0:dt*10:ts, q1(1,:))
% title('q1 Maps')
% 
% subplot(1,3,2)
% % plot(0:dt:ts, q1(2,:))
% plot(0:dt*10:ts, q1(2,:))
% title('q1 Salt')
% 
% subplot(1,3,3)
% % plot(0:dt:ts, q1(3,:))
% plot(0:dt*10:ts, q1(3,:))
% title('q1 Neither')

% suptitle('q1 = sum(proj(W)).^2')
% annotation('textbox', [0 0.9 1 0.1], ...
%     'String', 'q1 = sum(proj(W)).^2', ...
%     'EdgeColor', 'none', ...
%     'HorizontalAlignment', 'center')
%}

%{
% save('EqualLambdaR', '-v7.3')     

% dir = pwd;
% dir = [dir, '\'];
% fname = num2str(tau1);
% fname = ['Wprojections',fname];
% % exportpdf(1, fname, dir, [8.5, 1]) 
% % fname = strcat(num2str(nV1), 'neurons', num2str(ts));
% savefig(1, fname);% end



%     logicW = or(W >= Wmax, W <= 0);
    dW = dWdt(W)*dt;
    
    dW = dW - repmat(sum(dW,2),1,nThal)./nThal;
%     if ~any(any(logicW))
%         sW = sum(dW,2);
%         dW = dW - (1./nThal).*sW;
%     else 
%         sW = sum(dW,2)./sum(logicW,2);
%         dW = dW - sW;
%     end
    
    dW(W >= Wmax) = 0;
    dW(W <= 0) = 0;
    
    W = dW + W;
    %     sW = sum(W,2); % each synapse has a total synaptic strength that remains constant
    %     W = W - (1/(2*size(W,2)))*sW;
    W(W<0) = 0;
    W(W > Wmax) = Wmax;
    %     normTerm = sum(sum(or(W > 0, W < Wmax)));
%     if t > 1
    logicW = and(W <= Wmax, W >= 0);
%         if any(any(logicW))
%             normTerm = sum(logicW, 2);
%         end
%     elseif t == 1
%{
    if ~all(all(logicW))
        nW = (-sum(W(~logicW),2)+size(W,2))./sum(W(logicW),2);
%         nW = size(W,2)./sum(W,2);
    else
        nW = 1;
    end
    
        %     nW = (2*normTerm)./sW;
%     end
    W = nW.*W; 
%}
    %     W = W./norm(W);
    wtracker(:,:,t+1) = W;
%}

% nDim = 400; %number of neurons per dimension
% L = 10; % length of dimension (let's say the units are mm)
% d = 1; %sets the dimension of the neuron grid (1 2 or 3d)
% Assuming a square array -- could be adjust to be rectangular
% spacing = 2*L/(nDim); % units of mm (or whatever really)

% thalRates = 100; %in Hz, how fast Thalamic neurons fire
% nThal = 10*nV1; %number of Thalamus neurons
% ts = 10;%2*10^3; %max number of time steps simulation runs for 

% if d == 1
%     X = 1:nV1;
%     X = -L + (X-1)*spacing; %now units are mm
%     X = repmat(X, nV1, 1);
%     if PBC
%         X = bsxfun(@minus, X, X');
%         X(X > L) = 2*L - X(X > L) -spacing;
%         X(X < -L) = -2*L - X(X < -L) + spacing;
%         distV1 = X.^2;
%     else
%         distV1 = (bsxfun(@minus, X, X')).^2;
%     end
%     
% elseif d==2
%     [X, Y] = ind2sub([round(sqrt(nV1)), round(sqrt(nV1))], 1:nV1); %find the position (x,y) of each v1 neuron
%     % X  = (X -1 -floor(nDim/2))*(2*L/nDim);
%     % Y  = (Y - 1  - floor(nDim/2))*(2*L/nDim);
%     X = repmat(X, nV1,1);
%     Y = repmat(Y, nV1,1);
%     
%     distV1 = bsxfun(@minus, X, X').^2 + bsxfun(@minus, Y,Y').^2;
%     distV1 = (sqrt(distV1)*spacing).^2;
% end

%assuming a spiking network
% thalSpikes = rand(nThal, ts);
% thalSpikes(thalSpikes <= dt*thalRates) = 1;
% thalSpikes(thalSpikes ~= 1) = 0;

% finding a projection matrix

% aE = 1500*spacing; %30*spacing % scaling of excitatory gaussian
% aI = (1/3)*aE;%*spacing; % scaling of inhibitory gaussian
% af =  0.1*aE;%(20)*spacing;%0.03*aE;
% sigE = 20*spacing; % standard deviation of Exc gaussian (units = mm)
% sigE = 0.5;
% sigI = 1.5; %(5/2)*sigE;%0.25; % standard deviation of Inh Gaussian (units = mm)
% sigf = spacing;
% sigf = spacing/5;
% Gs = aE*exp(-distV1./(2*(sigE).^2)) - aI*exp(-distV1./(2*(sigI).^2)); % gives Gslow a Mexican (or cowboy hat) form
% Gf = af*exp(-distV1./(2*(sigf).^2));

% ReK = @(f) GE.*(abs(1 + 2*pi*1i*f/gE))^(-2) - GI.*(abs(1 + 2*pi*1i*f/gI))^(-2);
% ReK = @(f) GE.*heaviside(gE - f) - GI.*heaviside(gI - f);


    % @(W) ((lambda1*tau1)/4)*(((gE*(1-gE*tau1))/(1 - (gE*tau1)^2))*GE - (((gI*(1-gI*tau1))/(1 - (gI*tau1)^2)*GI)))*W*p1 ...
       % + ((lambda2*tau2)/4)*(((gE*(1-gE*tau2))/(1 - (gE*tau2)^2))*GE - (((gI*(1-gI*tau2))/(1 - (gI*tau2)^2)*GI)))*W*p2;

%        for t =1:nsteps
%     
%     dW = (1/tauW)*dWdt(W);
%     %         nAct = sum(and(W(cc,:) > 0, W(cc,:) < Wmax));
%     if NORM
%         dW(W >= Wmax) = 0;
%         dW(W <= 0) = 0;
%         nAct = sum(and(W < Wmax, W >0),2);
%         epsilon = sum(dW,2)./nAct;
%         epsilon = repmat(epsilon, 1, nThal);
%         epsilon(or(W >= Wmax, W <=0)) = 0;
%         dW = dW - epsilon;
%         W1 = dW*dt + W;
%         W1(W1 > Wmax) = Wmax;
%         W1(W1 < 0) = 0;
%         
%         Wact = sum(W1.*and(W1 < Wmax, W1 > 0),2);
%         Wfroz = sum(W1.*or(W1 >=Wmax, W1<= 0),2);
%         
%         
% %         for cc = 1:nV1
% %             Wact(cc) = sum(W1(cc, dW(cc,:)~=0));
% %             Wfroz(cc) = sum(W1(cc, dW(cc,:)==0));
% %         end
%        
%         gamma = (-Wfroz + nThal)./Wact;
%         gamma = repmat(gamma, 1, nThal);
%         
%         
%         gamma(or(W1 >= Wmax, W1<=0)) = 1;
%         W = W1.*gamma;
%     elseif ~NORM
%         W = dW*dt + W;
% %         W = W/norm(W);
%     end
%     
% %     wp1(:,t+1) = W*e1;
% %     wp2(:,t+1) = W*e2;
% %     wp3(:,t+1) = W*e3;
% %     
% %     q1(1,t+1) = sum(W*e1).^2;
% %     q1(2,t+1) = sum(W*e2).^2;
% %     q1(3,t+1) = sum(W*e3).^2;
%     
%     if mod(t,10) == 0
%         %      wtracker(:,:,t+1) = W;
%         wp1(:,(t/10)+1) = W*e1;
%         wp2(:,(t/10)+1) = W*e2;
%         wp3(:,(t/10)+1) = W*e3;
%         
%         q1(1,(t/10)+1) = sum(W*e1).^2;
%         q1(2,(t/10)+1) = sum(W*e2).^2;
%         q1(3,(t/10)+1) = sum(W*e3).^2;
%         
%     end
% end

% m.q1 = mean(maps.^2); %m structure represents maps; q1 is variance always
% m.q2 = mean(diff(maps).^2)/m.q1; %q2 is nearest neighbors jumps
% m.q3 = mean(maps);
% 
% s.q1 = mean(salt.^2);
% s.q2 = mean(diff(salt).^2)/s.q1;
% s.q3 = mean(salt);
% 
% n.q1 = mean(neither.^2);
% n.q2 = mean(diff(neither).^2)/n.q1;
% n.q3 = mean(neither);
