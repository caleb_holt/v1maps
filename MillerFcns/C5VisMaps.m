% function [Won, Woff, WRFon, WRFoff, Inp] = C3VisMapsMillerRec(rc,rI, ii)  %visualMaps(tau1, nDim) 
% [m, s, n, lTrue, growth,wp, W, Inp] = YasharVisMaps(10, 0.67, filenumber) --
% where filenumber is just used to save a file. 
% the third element of both lTrue and growth is the ratio of lTrue1/lTrue2,
% similar for growth.
%lambda2 multiplies the fast component of C(f). 
%af = alpha_f and multiplies the fast component of Gf. 
% 

%including feedback connections. 


%MillerRec_2Dgrid_Norm_fft_Miller26 is a good recreation of fig 6 in Miller. 
%MillerRec_2Dgrid_Norm_fft_Miller27 is a good recreation of fig 8 (I 0.3). 
%MillerRec_2Dgrid_Norm_fft_Miller28 is a good recreation of fig 8 (E 0.3). 
% MyFrame_2Dgrid_Norm_fft_Miller1-5 use Gf = 0, so
% MyFram_2Dgrid_Norm_fft_Miller6.mat uses lambda1 = 0, and makes a S&P map
% for both ori and phase. 
% MyFrame_2Dgrid_Norm_fft_Miller7.mat uses lambda1 = 1 and makes a quasi
% smooth map for both phase and ori. 
% MyFrame_2Dgrid_Norm_Miller10.mat uses projection matrices instead of
% correlation matrices, and produces some noisey RF's. 
% MyFrame_16 uses Besselj(0, sqrt(k*distV1sq) for the Cnn_fast
% MyFrame_17 uses besselj(0, x) for both Cnn_fast and Cnn. '
% MyFrame_18 uses sE1, sI1, etc values for sEc, etc scaled by spacing (or
% dx) -- No bessels 
% MyFrame_23 uses MH, but a very broad MH for Cnn_fast. This localizes it
% in Fourier space and keeps overlap between Cnn_fast and Cnn low -- still
% gives smooth maps for both. 
% MyFrame_25 uses Bessels forboth and does weird stuff. 

%Feedback_2Dgrid_Norm_fft_Miller1.mat uses their (Antolik and Bednar) P and my K
%Feedback_2Dgrid_Norm_fft_Miller2.mat uses their K and P. 
%Feedback_3 is the one that gives the best results, smooth for ori and S&P
%for phase. It doesn't use the right arbor or scales I think. 
%Feedback_4 is using the right number of neurons and arbor, but the wrong
%scales for the Gaussians and different L
%Feeback_5 uses the right scales, L, and arbors. Doesn't work though. 
%Feedback_11 interpolates between Miller and Antolik with MvA = 1 (pure
%Antolik)
%Feedback_12 "                                               " MvA = 0.5
%Feedback_13 "                                               " MvA = 0
%(pure Miller)
%Feedback_14 uses MvA=0 on a 32x32 grid to recapitulate Miller's results
%as a sanity check.
%Feedback_15 interpolates between Miller and Antolik with MvA = 0 (pure
%Antolik)
%Feedback_16 "                                               " MvA = 0.5
%Feedback_17 "                                               " MvA = 1
% 15-17 uses K as Miller defines it in P. Don't really work.  

%Sparse 1-9 all look weird and give good low correlations between cells,
%but do not give maps for one and S&P for the other. 
%Sparse_10 uses sparsity = 1 (so not sparse), but shows that I didn't screw
%things ups by putting in Aon and Aoff. 
%Sparse_11 uses sparsity = 0.5 and different Arbors (Aon & Aoff) but to no
%avail. Both maps become S&P
%Sparse_12 uses sparsity = 0.75, still no luck. I'm moving back to
%considering feedback connections. 

clear
ii = 29;
rc = 0.20; 
rI = 0.3; 
c_a = 0.5; %this is the factor that scales the dendritic radius

Inp.rc = rc;
Inp.rI = rI;

lambda2 = 48 %.24; %25/3.5;
af = 0.67;%0.67;%0.3

%To easily switch between my notation and Miller's will be difficult. But here we go. I have to scale my aE and aI by the normalization constant so that the gaussians are now normalized I think
%I also have to set L = 16, so that the dx = 1, and the sigma's he uses are
%different from the ones I had previously defined, but not hugely. 

%NOTE THIS IS DIFFERENT IN THAT IT USES ON & OFF SYNAPSES. 

SPARSE = 0; %determines if we use W as a sparse (SPARSE = 1) or not (SPARSE = 0) matrix
NORM = 1; %Doesn't normalize W (Norm=0), or uses Miller norm (NORM = 1);
ARBOR = 1; %uses the arbor function or doesn't (ARBOR = 0). 
ONOFF = 0; % whether to use ON and OFF cells for LGN (ONOFF = 0, don't use).
PBC = 1;%1 = impose periodic B.C.'s
MILL94 = 1;
IntMethod = 'Miller'; %My previous results were using the Euler method. 
fftpath = 'yes'; %Switch between fft ('yes') and not ('brute-force'). \

d = 2;%topographic/retinotopy dimension 

MvA = 0; % linearly interpolates between Miller and Antolik,  0 = Miller purely, 1 = Antolik purely
Inp.MvA = MvA;

if d==1
    L = 10
    nDim = 1024;%256; %Miller 1994 uses 1024 neurons in a 32x32 grid
    R_arbor = 1; %sets length of arbor
   
elseif d==2
    nDim = 32; % want this to be 16
    L = 16 %Antolik length = 1 unit/side; %nDim/2 %0.3125 %To make spacing the same for 1D case and 2D case
    R_arbor = 6*(2*L/nDim);% 4 * dx -- Gives me a diameter of 11, Miller has an effective diameter of 11.3. 
%     R_arbor = 0.2*(2*L/nDim)/(1/96); %19 %from Antolik Paper
end

D_arbor = 2*floor(R_arbor/(2*L/nDim))+1; %11; 
nDimV1 = nDim;
nDimLGN = nDim; %But there are two grids of LGN cells (ON and OFF center cells)
dx = 2*L/(nDim);
nV1 = nDim^d; %number of V1 neurons -- one dimensional currently
nThal = nV1; %sets size of input layer. =nV1 means equal number of input and output neurons

eps = 0; %sets scale of epsilon. Set eps=0 to not include order epsilon terms in dW/dt

aE = 1; %this is one in Miller 94
aI = 1/9;


a_s = 0.5; % multiplies the G's (interaction term) by something less than 1 for distances greater than 0;
ac = 1/9;


sigE = 6.5*rI/sqrt(2)*dx; %0.5; %These are the Gs sigmas; Sqrt(2) is there cause he defines his gaussian's sigmas with the 2. His notation- exp[-(x/sigma)^2]. So multiply by 1/sqrt(2) to equate our notations
sigI = sqrt(1/aI)*sigE; 
sigf = 0.01; % this is the Gf sigma

%{
sEc = 15.36;
sIc = 51.2;
sEc2 = 1.7067;
sIc2 = 5.12;
%}

% sEc = (1/sqrt(2))*rc*D_arbor/2;%0.3; % defines the correlations sigmas -- I think these are the problem.
sEc = (1/sqrt(2))*rc*D_arbor/2*dx;
sIc = sqrt(1/ac)*sEc; %similar reason for sqrt(2) factor. 
% sEc2 = sEc*10; %sEc/9;%0.1/3; % got this from Analyzing things in CorrEig;
% sIc2 = sIc*10;%0.3/3;% divide by 3 so the fast correlation oscillates more

sEc2 = sEc/10;
sIc2 = sIc/10;

tau1 = 100;
tau2 = 1;
gs = 1/tau1; %this is gamma_s from Yashar's notes -- arbitrarily setting gamma_s = 1/tau1
gf = 1/tau2; % this is gamma_f from Yashar's notes

lambda1 = 1; %lambda2 is given as first input to function. (normally 10);

dt = 0.1*tau2; % dt sets the time scale -- currently 1/10 of tau2.
tauW = 1000;
scaleTauW = tauW/(lambda1*aE);
T = round(5*10*tauW/(lambda1*aE));
%=========================================================================
Inp.L = L;
Inp.d = d;
Inp.nDim = nDim;
Inp.nDimV1 = nDimV1;
Inp.nDimLGN = nDimLGN;
Inp.sigE = sigE;
Inp.sigI = sigI;
Inp.sE1 = sEc;
Inp.sI1 = sIc;
Inp.sE2 = sEc2;
Inp.sI2 = sIc2;
Inp.aE = aE;
Inp.aI = aI;
Inp.spacing = dx;
Inp.PBC = PBC; %determines if we're using Periodic Boundary conditions (PBC = 1) or not (PBC = 0)
Inp.Norm = NORM;
Inp.eps = eps;
Inp.nV1 = nV1;
Inp.nThal = nThal;%floor(nV1/2);
Inp.lambda1 = lambda1; 
Inp.lambda2 = lambda2;
Inp.tau = [tau1; tau2; tauW]; %tau1, tau2, tauW defined in ConDefinitions.mat
Inp.scaleTau = tauW/(lambda1*aE);
Inp.fftpath = fftpath;
Inp.IntMethod = IntMethod;
Inp.c_a = c_a; 

switch IntMethod
    case 'Euler'
        Inp.T = T;
        Inp.Nsteps = round(T/dt);
end

%=========================================================================
%get distances on the grid (in d==1, distV1 gives distances between all pairs of grid-points, in d==2, it gives distances of all grid-points with grid-point at (-L,-L):
useGdist = 1; %useGlobalDistance 
[distV1sq, Xdist] = neurDistMill(nDim, d, L,PBC, 0); %putting a random number as the 5th variable uses globaldist to find the distance. So distV1sq is then nV1 x nThal instead of nDimV1 x nDimLGN
distV1 = sqrt(distV1sq);
Inp.distV1 = distV1;
Inp.Xdist = Xdist; 


[gdist, ~] = neurDistMill(nDim, d, L, PBC, useGdist); %this way I can use gdist for creating the W so that it's big enough 
gdist = sqrt(gdist); 
%--------------------------------------------------------
%make arbor function
if ARBOR 
	ANTOLIK = 0;
    sparsity = 1;
    [A,~,~,RFinds,Nx_arbor] = Arbor2(Xdist, R_arbor, sparsity, d, nDim,nDim,L, c_a, ANTOLIK); %R_arbor = radius of the arbor, c_a = scale factor for the other circle (see Miller 94)
    
    if sparsity ~= 1
        maskON = rand(size(A));
        maskON(maskON < sparsity) = 1;
        maskON(maskON ~= 1) = 0;
        
        maskOFF = rand(size(A));
        maskOFF(maskOFF < sparsity) = 1;
        maskOFF(maskOFF ~= 1) = 0;
        
        Aon = A.*maskON;
        Aoff = A.*maskOFF;
    else
        Aon = A;
        Aoff = A;
    end
    
    
elseif ~ARBOR
    A = ones(nV1,nThal);
    Nx_arbor = nThal;
    RFinds = (1:nV1*nThal)';
end

if ONOFF
    A = [A,A];
end

Inp.sparsity = sparsity;
% nRF = sum(A(1,:)~=0);%number of presynaptic inputs to a V1 cell, also W is initialized and normalized such that sum(W,2) stays equal to nRF
% nRF = 2*nRF; %for the two kinds of cells. 

nRF = sum(Aon(:)~=0) + sum(Aoff(:) ~=0);

%--------------------------------------------------------
%make p's or C's
% currently assuming same grid (resolution) in LGN as in V1 
if d==1
    if ARBOR
        Inp.stc = 'corr1';
        p1_pars.sigEI = [sEc,sIc];
        p1_pars.aEI = [aE,aI];
        p2_pars.sigEI = [sEc2, sIc2];%0.5 * p1_pars.sigEI;
        p2_pars.aEI = p1_pars.aEI;
        [p1, p2, e1,e2, e3] = CorrProj(distV1, nThal, Inp.stc, sEc, sIc, sEc2, sIc2, aE, aI, 1);%3,5,spacing/5, spacing,  sigE, sigI, 0.5*sigE, 0.5*sigI,
        
       
        [e1RF,e2RF,e3RF] =  RFcovPCs(dx,d,p1_pars,p2_pars, R_arbor); %RF-sized vectors
        
    else
        Inp.stc = 'proj';
        [p1, p2, e1,e2, e3] = CorrProj(distV1, nThal, Inp.stc, sigE, sigI, 0.5*sigE, 0.5*sigI, aE, aI, 1);%3,5,spacing/5, spacing
    end
elseif d==2 
    if ~ONOFF
        switch fftpath
            case 'yes'
                Inp.stc = 'corr2D';
                k = 2*pi; %*13;
                %         [Cnn, Cnn_fast, e1, e2, e3] = CorrProj(distV1, nThal, Inp.stc);
                Cnn = makeMexHat2(distV1, sEc, aE, sIc, ac,d, MILL94); % Cnn = C_on,on.
%                 Cnn = 2*pi*besselj(0, sqrt(k*distV1sq));
%                 Cnn = Cnn./(max(max(fft2(Cnn))));
                Cff = Cnn; % C_off,off
                Cnf = -0.5*Cnn; % C_on, off
                Cfn = Cnf;
                [e1, s] = svds(Cnn, 1);
                
%                 theta = (0:10:350)*pi/180;
%                 k_x = k*cos(theta);
%                 k_y = k*sin(theta);
%                 Ydist = Xdist';
%                 Cnn_fast = zeros(size(Xdist));
%                 
%                 for tt = 1:length(theta)
%                     Cnn_fast = cos(k_x(tt)*Xdist + k_y(tt)*Ydist)+sin(k_x(tt)*Xdist + k_y(tt)*Ydist) + Cnn_fast;
%                 end
                
                Cnn_fast = makeMexHat2(distV1, sEc2, aE, sIc2, ac,d, MILL94); %ac
%                 Cnn_fast = ifft2(and(distV1 > 14, distV1 <= 15));
%                 Cnn_fast = 2*pi*besselj(0, sqrt(0.3*k*distV1sq));
%                 Cnn_fast = Cnn_fast./(max(max(fft2(Cnn_fast))));
%                 Cnn_fast = ifft2(distV1 <= 1);
                Cff_fast = Cnn_fast;
                Cnf_fast = -0.5*Cnn;
                Cfn_fast = Cnf_fast;
                [e2, s2] = svds(Cnn_fast,1);
                
                CD = Cnn - Cnf;
            case 'brute-force'
                Inp.stc = 'corr2D';
                k = 2*pi; %*13;
                Cnn = makeMexHat2(distV1, sEc, aE, sIc, ac,d, MILL94); % Cnn = C_on,on.
%                 Cnn = 2*pi*besselj(0, sqrt(k*distV1sq));
%                 Cnn = Cnn./(max(max(fft2(Cnn))));
                Cff = Cnn; % C_off,off
                Cnf = -0.5*Cnn; % C_on, off
                Cfn = Cnf;
                [e1, s] = svds(Cnn, 1);
                
                Cnn_fast = makeMexHat2(distV1, sEc2, aE, sIc2, ac,d, MILL94);
                Cff_fast = Cnn_fast;
                Cnf_fast = -0.5*Cnn;
                Cfn_fast = Cnf_fast;
                [e2, s2] = svds(Cnn_fast,1);
                
                CD = Cnn - Cnf;
                                
%                 Inp.stc = 'corr2D';
%                 [Cnn, Cnn_fast, e1, e2, e3] = CorrProj(distV1, nThal,'proj');
%                 Cff = Cnn; % C_off,off
%                 Cnf = -0.5*Cnn; % C_on, off
%                 Cfn = Cnf;
%                 
%                 Cff_fast = Cnn_fast;
%                 Cnf_fast = -0.5*Cnn;
%                 Cfn_fast = Cnf_fast;
        end
                
                %
                %         if abs(e1'*e2) > 0.001
                %             error('e1 and e2 not orthogonal enough');
                %         end
                
                %{
        p1_pars.sigEI = [sEc, sIc];%[1,1];
        p1_pars.aEI = [aE, aI];
        p2_pars.sigEI = [sE2, sI2];%0.5 *[1,1];
        p2_pars.aEI = [aE,aI];
        [p1, p2,e1,e2,e3] = CorrProj2D(distV1, p1_pars,p2_pars, R_arbor);%3,5,spacing/5, spacing
        [e1RF,e2RF,e3RF] =  RFcovPCs(dx,d,p1_pars,p2_pars, R_arbor);
        %}
    else
        Inp.stc = 'corr2D_ONOFF';
        p1same_pars.sigEI = [1,1];
        p1same_pars.aEI = [1,1];
        p2same_pars.sigEI = 0.5 *[1,1];
        p2same_pars.aEI = [1,1];
        [p1_same, p2_same] = CorrProj2D(distV1, p1same_pars,p2same_pars, R_arbor);%3,5,spacing/5, spacing
        [p1_diff, p2_diff] = CorrProj2D(distV1, p1diff_pars,p2diff_pars, R_arbor);%3,5,spacing/5, spacing
        [e1,e2,e3] =  RFcovPCs(dx,d,sigE*[1,0.5], sigI*[1,0.5], aE*[1,1], aI*[1,1], R_arbor);
    end
end

Inp.A = A;
Inp.Aon = Aon;
Inp.Aoff = Aoff;
Inp.R_arbor = R_arbor;
Inp.nRF = nRF;
Inp.Nx_arbor = Nx_arbor;
Inp.RFinds = RFinds;
Inp.Cnn = Cnn;
Inp.Cff = Cff;
Inp.Cnn_fast = Cnn_fast;
Inp.e1 = e1;
Inp.e2 = e2;

% Inp.e3 = e3;
% Inp.e1RF = e1RF;
% Inp.e2RF = e2RF;
% Inp.e3RF = e3RF;
% Inp.endCondition = 0.1*nRF*nV1; % Come back to this in a second
Inp.endCondition = 0.1*nRF; 
%--------------------------------------------------------
%make G's 
Gs = makeMexHat2(distV1,sigE,aE,sigI,aI,d, MILL94);
Gf = makeMexHat2(distV1,sigf,af,0,0,d, MILL94);
Gs(distV1 ~= 0) = a_s*Gs(distV1~=0);
% Gf = 0; 
Inp.Gs = Gs;
Inp.Gf = Gf;

% G1 = 0.5*Gs + tau1/(tau1+tau2)* Gf;
% G2 = tau2/(tau2+tau1)* Gs + 0.5* Gf;

% G1 = eye(size(Gs));
G1 = 0.5*Gs + tau1/(tau1+tau2)* Gf;
G1 = Gs; 
% G2 = zeros(size(Gs)); 
G2 = tau2/(tau2+tau1)* Gs + 0.5* Gf;
K = G1 + G2;
K = G1;
% CorrPlots(nDim, A, p1, p2, G1, G2)
Inp.G1 = G1;
Inp.G2 = G2;
Inp.K = K;
%--------------------------------------------------------

[P, Inp] = makeFeedbackConnections(Inp, K, distV1); 
lambdaP = 1; %8.7;
P = lambdaP*P;

% P = zeros(size(K));

Inp.lambdaP = lambdaP;
Inp.P = P;
%--------------------------------------------------------

%make the pure (unnormalized/unconstrained) dWdt 
switch Inp.stc
    case {'proj','corr1'}
        switch fftpath
            case 'brute-force'
                dWdt = @(W) 1/4 * A.*( lambda1*G1*W*p1 + lambda2*G2*W*p2 + eps*(gs*Gs + gf*Gf)*W ); %this adds a more complete term. ;
                lambda1fft = max(real(lambda1*fft(G1(1,:)')))
                lambda2fft = max(real(lambda2*fft(G2(1,:)')))
                Inp.lambda1fft = lambda1fft;
                Inp.lambda2fft = lambda2fft;
                
            case 'yes' %currently this case assumes eps is 0
                if d~=1
                    error('d must be 1, in case corr1');
                end
                GCff_fft = fft(G1(1,:)') * fft(p1(1,:)); %nV1 x nThal matrix
                GCnf_fft = fft(G2(1,:)') * fft(p2(1,:)); %nV1 x nThal matrix
                dWdt = @(W) 1/4 * fastdWdt2(W,A, lambda1*GCff_fft + lambda2*GCnf_fft);
                lambda1fft = max(real(lambda1*GCff_fft(:)))
                lambda2fft = max(real(lambda2*GCnf_fft(:)))
                Inp.lambda1fft = lambda1fft;
                Inp.lambda2fft = lambda2fft;
        end
    case 'corr2D' %currently this case assumes eps is 0
        switch fftpath
            case 'yes'
%                 on cells
%                 G1fft = reshape(G1(:,1), nDimV1, nDimV1); %gets first matrix describing connectivity between (-L, -L) neuron and rest of network in V1
                G1fft = reshape(fft2(G1), nV1,1); %takes the fft in 2d and then vectorizes the results
                G2fft = reshape(fft2(G2), nV1, 1);
                
                Pfft = reshape(fft2(P), nV1, 1); 
                
%                 Pfft = Pfft./max(Pfft);
                
%                 Cnnfft = reshape(Cnn(:,1), nDimLGN, nDimLGN); %gets first matrix describing connectivity between (-L, -L) neuron and the rest of network in Thalamus
                Cnnfft = reshape(fft2(Cnn),nThal, 1); %takes the fft in 2d and then vectorizes the results
                fastCnn_fft = reshape(fft2(Cnn_fast), nThal,1);
%                 Cnffft = reshape(Cnf(:,1), nDimLGN, nDimLGN); %gets first matrix describing connectivity between (-L, -L) neuron and the rest of network in Thalamus
                Cnffft = reshape(fft2(Cnf),nThal, 1);
                fastCnf_fft = reshape(fft2(Cnf_fast), nThal,1);
                
                GCnn_fft = kron(Cnnfft, G1fft); %takes the (dirac? tensor?) product of the two. So now dimensionality is (nV1*nThal)x1
                PCnn_fft = kron(Cnnfft, Pfft); %I'm only including this term right now, need to think about how to take integral w.r.t df to really get P term
                fastGCnn_fft = kron(fastCnn_fft, G2fft);
                lambda1fft_on = max(real(lambda1*GCnn_fft))
                lambda2fft_on = max(real(lambda2*fastGCnn_fft))
                lambdaPfft_on = max(real(PCnn_fft))
                
                GCnf_fft = kron(Cnffft, G1fft);
                PCnf_fft = kron(Cnffft, Pfft);
                fastGCnf_fft = kron(fastCnf_fft, G2fft);
                
                GCnn_fft = reshape(GCnn_fft,[nDimV1,nDimV1,nDimLGN,nDimLGN]); %think of this as like 2 2-vector indices. In the 1D case, those 2-vectors become 1-vectors. 
                GCnf_fft = reshape(GCnf_fft, [nDimV1,nDimV1,nDimLGN,nDimLGN]);
                PCnn_fft = reshape(PCnn_fft, [nDimV1,nDimV1,nDimLGN,nDimLGN]);
                PCnf_fft = reshape(PCnf_fft, [nDimV1,nDimV1,nDimLGN,nDimLGN]);
                fastGCnn_fft = reshape(fastGCnn_fft, [nDimV1,nDimV1,nDimLGN,nDimLGN]);
                fastGCnf_fft = reshape(fastGCnf_fft, [nDimV1,nDimV1,nDimLGN,nDimLGN]);
                
                %dWdton = @(W) 1/4 * fastdWdt2(W,A, GCn_fft + GC2fft, nDimV1, nDimLGN);%,nDimV1,nDimLGN);
                dWdton = @(Won, Woff) 1/4 *(fastdWdt2(Won, Aon, (1-MvA)*(lambda1*GCnn_fft+lambda2*fastGCnn_fft) + MvA*PCnn_fft, nDimV1, nDimLGN) + fastdWdt2(Woff, Aon, (1-MvA)*(lambda1*GCnf_fft+lambda2*fastGCnf_fft) + MvA*PCnf_fft, nDimV1, nDimLGN));
%                 G1fft = reshape(fft2(G1),nV1, 1);
%                 CDfft = reshape(fft2(CD),nThal,1);
%                 GCfft = kron(CDfft, G1fft);
%                 lambda1fft = max(real(lambda1*GCfft));
%                 GCfft = reshape(GCfft, [nDimV1, nDimV1, nDimLGN, nDimLGN]);
%                 
%                 dWdt = @(W) 1/4 * fastdWdt2(W, A, GCfft, nDimV1, nDimLGN);
%                 dWdton = dWdt;
%                 dWdtoff = dWdt; 
                
                
                %off cells
%                 Cff_fft = reshape(Cff(:,1), nDimLGN, nDimLGN); %gets first matrix describing connectivity between (-L, -L) neuron and the rest of network in Thalamus
                Cff_fft = reshape(fft2(Cff),nThal, 1); %takes the fft in 2d and then vectorizes the result;
                fastCff_fft = reshape(fft2(Cff_fast), nThal,1);
%                 Cfn_fft = reshape(Cfn(:,1), nDimLGN, nDimLGN); %gets first matrix describing connectivity between (-L, -L) neuron and the rest of network in Thalamus
                Cfn_fft = reshape(fft2(Cfn),nThal, 1);
                fastCfn_fft = reshape(fft2(Cfn_fast), nThal, 1);
                
                GCff_fft = kron(Cff_fft, G1fft); %takes the (dirac? tensor?) product of the two. So now dimensionality is (nV1*nThal)x1
                PCff_fft = kron(Cff_fft, Pfft);
                fastGCff_fft = kron(fastCff_fft, G2fft);
                lambda1fft_off = max(real(lambda1*GCff_fft))
                lambda2fft_off = max(real(lambda2*fastGCff_fft))
                lambdaPfft_off = max(real(PCff_fft)) 
                
                GCfn_fft = kron(Cfn_fft, G1fft);
                PCfn_fft = kron(Cfn_fft, Pfft);
                fastGCfn_fft = kron(fastCfn_fft, G2fft);
                
                GCff_fft = reshape(GCff_fft,[nDimV1,nDimV1,nDimLGN,nDimLGN]); %think of this as like 2 2-vector indices. In the 1D case, those 2-vectors become 1-vectors. 
                fastGCff_fft = reshape(fastGCff_fft,[nDimV1,nDimV1,nDimLGN,nDimLGN]);
                PCff_fft = reshape(PCff_fft, [nDimV1,nDimV1,nDimLGN,nDimLGN]);
                PCfn_fft = reshape(PCfn_fft, [nDimV1,nDimV1,nDimLGN,nDimLGN]);
                GCfn_fft = reshape(GCfn_fft, [nDimV1,nDimV1,nDimLGN,nDimLGN]);
                fastGCfn_fft = reshape(fastGCfn_fft,[nDimV1,nDimV1,nDimLGN,nDimLGN]);
                
%                 dWdtoff = @(W) 1/4 * fastdWdt2(W,A, GCff_fft + GCfn_fft, nDimV1, nDimLGN);
                dWdtoff = @(Woff, Won) 1/4 *(fastdWdt2(Woff,Aoff, (1-MvA)*(lambda1*GCff_fft+lambda2*fastGCff_fft) + MvA*PCff_fft, nDimV1, nDimLGN) + fastdWdt2(Won,Aoff, (1-MvA)*(lambda1*GCfn_fft+lambda2*fastGCfn_fft) +MvA*PCfn_fft, nDimV1, nDimLGN));
                
                Inp.lambda1fft_on = lambda1fft_on;
                Inp.lambda2fft_on = lambda2fft_on;
                Inp.lambda1fft_off = lambda1fft_off;
                Inp.lambda2fft_off = lambda2fft_off;
            
            case 'brute-force'
                 
                dWdton = @(Won, Woff) 1/4 * Aon.*((lambda1*(1-MvA)*G1+ lambdaP*MvA*P)*(Won*Cnn + Woff*Cnf)+ lambda2*G2*(Won*Cnn_fast + Woff*Cnf_fast));
                dWdtoff = @(Woff, Won) 1/4 * Aoff.*((lambda1*(1-MvA)*G1+ lambdaP*MvA*P)*(Woff*Cff + Won*Cfn)+ lambda2*G2*(Woff*Cff_fast + Won*Cfn_fast));
                
%                 dWdt = @(W) 1/4 * A.*( lambda1*G1*W*p1 + lambda2*G2*W*p2 + eps*(gs*Gs + gf*Gf)*W );
                lambda1fft_on = max(max(real(lambda1*fft2(G1))));
                lambda2fft_on = max(max(real(lambda2*fft2(G2))));
                lambdaPfft_on = max(real(lambdaP*fft2(P)));
                
                Inp.lambda1fft_on = lambda1fft_on
                Inp.lambda2fft_on = lambda2fft_on
                Inp.lambdaPfft_on = lambdaPfft_on
                
                lambda1fft_off = max(max(real(lambda1*fft2(G1))));
                lambda2fft_off = max(max(real(lambda2*fft2(G2))));
                lambdaPfft_off = max(real(lambdaP*fft2(P)));
                
                Inp.lambda1fft_off = lambda1fft_off;
                Inp.lambda2fft_off = lambda2fft_off;
                Inp.lambdaPfft_off = lambdaPfft_off;
        end
end

% Inp.lambda1fft = lambda1fft;
% Inp.lambda2fft = lambda2fft;
%--------------------------------------------------------
% set the stopping time


 dt = 0.1;
% tauW = 100;

% ts = 5*10*tauW/(lambda1*aE); % to run for long enough time so that dynamics are well-developped.
% ts = 100*ts;
% nsteps = round(ts/dt)

% nsteps = 200

% Inp.totalTime = ts;
% Inp.Nsteps = nsteps;


%--------------------------------------------------------


%--------------------------------------------------------
%set the initial condition for W
Wmax = 4;
Wmean = 1;
Wnoise = 0.2;
% sigW = 0.2*dxConv;d %*96; %this is from Antolik and Bednar paper

Won = Wmean + Wnoise*(2*rand(nV1, nThal)-1);%Uniform distribution on [Wmean-Wnoise, Wmean+Wnoise];
% Wbase = exp(-gdist.^2./(2*sigW^2));
% Won = rand(nV1, nThal).*Wbase; %Consistent with Antolik and Bednar

Won = Aon.*Won;

Woff = Wmean + Wnoise*(2*rand(nV1, nThal)-1);%Uniform distribution on [Wmean-Wnoise, Wmean+Wnoise];
% Woff = rand(nV1, nThal).*Wbase; %consistent with Antolik and Bednar
Woff = Aoff.*Woff;

if NORM %rescale to set the sum of weights to V1 cells equal to sum(A,2)
%     W1on = repmat(2*sum(A,2)./sum(Won+Woff,2) ,1, nThal).*Won; %see Miller Algorithm step 5. 
%     Woff = repmat(2*sum(A,2)./sum(Won+Woff,2),1,nThal).*Woff; %the 2 is cause there are now two grids of LGN
    W1on = repmat(sum(Aon+Aoff,2)./sum(Won+Woff,2) ,1, nThal).*Won; %see Miller Algorithm step 5. 
    W1off = repmat(sum(Aon+Aoff,2)./sum(Won+Woff,2),1,nThal).*Woff; %the 2 is cause there are now two grids of LGN
    
    Won = W1on;
    Woff = W1off;
end

Inp.Won = Won;
Inp.Woff = Woff;
Inp.Wmax = Wmax;
%--------------------------------------------------------

% wp1 = zeros(nV1, (nsteps/100)+1); % I don't save every times step so wp's stay smaller in size
% wp2 = wp1;
% wp3 = wp1;
% wp1RF = wp1;
% wp2RF = wp1;
% wp3RF = wp1;
% ts_wp = wp1(1,:);
% 
% 
% WRF = W';%when vectorized LGN-indices for the same V1-cell are consecutive now
% WRF = reshape(WRF(RFinds),[Nx_arbor^d,nV1])';%get square-RF's of all V1 cells.
% 
% wp1RF(:,1) = WRF*e1RF;%"selectivity"/overlap of V1 cell RF's for/with e1
% wp2RF(:,1) = WRF*e2RF;%"selectivity"/overlap of V1 cell RF's for/with e2
% wp3RF(:,1) = WRF*e3RF;%"selectivity"/overlap of V1 cell RF's for/with e2
% wpRF(:,:,1) = wp1RF;
% wpRF(:,:,2) = wp2RF;
% wpRF(:,:,3) = wp3RF;
% 
% wp1(:,1) = W*e1;
% wp2(:,1) = W*e2;
% wp3(:,1) = W*e3;
% wp(:,:,1) = wp1;
% wp(:,:,2) = wp2;
% wp(:,:,3) = wp3;

        

%========================================================================

%run Hebbian learning

if NORM
    %     while sum(sum(or(W >= Wmax, W <= 0),2)) ~= nV1*nThal % could do it
    %     this way to make all synapses inactive or
    switch IntMethod
        case 'Miller'
            [Won,Woff, WRFon,WRFoff, ts_wp, nsteps, timeUp,dt] = MillerInt(Won, Woff, dWdton,dWdtoff, Inp);
            
            Inp.Nsteps = nsteps;
            Inp.dt = dt;
            
        case 'Euler'
%             [Won, wp, wpRF, ts_wp, timeUp] =  EulerMethod(Won, dWdt, Inp, dt,ts);
            [Won,Woff, WRFon,WRFoff, ts_wp, timeUp] =  EulerMethod(Won,Woff, dWdton,dWdtoff, Inp, dt, T);
%             wp1 = wp(:,:,1);
%             wp2 = wp(:,:,2);
%             wp3 = wp(:,:,3);
%             wp1RF = wpRF(:,:,1);
%             wp2RF = wpRF(:,:,2);
%             wp3RF = wpRF(:,:,3);
            Inp.dt = dt;
            
    end
Inp.endT = timeUp;

elseif ~NORM
    for t = 1:nsteps
        Won = Won +  (dt/tauW)*dWdt(Won);        
        
        if mod(t,10) == 0
            %      wtracker(:,:,t+1) = W;
            WRF = Won';
            WRF = reshape(WRF(RFinds),[Nx_arbor^d,nV1])';
            wp1(:,(t/10)+1) = WRF*e1;
%             wp2(:,(t/10)+1) = WRF*e2;
%             wp3(:,(t/10)+1) = WRF*e3;
            ts_wp((t/10)+1) = t*dt;
        end
    end
Inp.endT = t;    
end

%========================================================================


% WRFon = Won';
% WRFon = reshape(WRFon(RFinds),[Nx_arbor^d,nV1])';%size(WRF) = [nV1,Nx_arbor^d]
% WRFoff = reshape(WRFoff(RFinds),[Nx_arbor^d,nV1])';
if d==2    
    %WRF = reshape(WRF,nV1,Nx_arbor,Nx_arbor);%all RF's
    WRFploton = reshape(WRFon,nDimV1,nDimV1,Nx_arbor,Nx_arbor);%all RF's
    WRFploton = permute(WRFploton, [3,1,4,2]);
    WRFploton = reshape(WRFploton, [nDimV1*Nx_arbor , nDimV1*Nx_arbor]);
    
    WRFplotoff = reshape(WRFoff,nDimV1,nDimV1,Nx_arbor,Nx_arbor);%all RF's
    WRFplotoff = permute(WRFplotoff, [3,1,4,2]);
    WRFplotoff = reshape(WRFplotoff, [nDimV1*Nx_arbor , nDimV1*Nx_arbor]);
    
end

figure(1)
imagesc(WRFploton)
colorbar
title('On Cells WRF')

figure(2)
imagesc(WRFplotoff)
colorbar
title('Off Cells WRF')

figure(3)
imagesc(WRFploton-WRFplotoff)
colormap('gray');
colorbar('Ticks', [min(min(WRFon-WRFoff)),max(max(WRFon-WRFoff))], 'TickLabels', {'Off', 'On'})
title('Difference between On & Off')
%}
[PrefTheta, PrefK, PrefPhi, zThetaSel, zThetaPref, zPhiSel, zPhiPref] = RFmeasure(Inp, WRFon, WRFoff);

spatialCorrRF = RFcorr(Inp, WRFon, WRFoff);

[DThetaNeighbors, DThetaRandos, DPhiNeighbors, DPhiRandos] = diffPrefTheta(Inp, zThetaPref, zPhiPref);

%Define Results Structure....................
Results.PrefTheta = PrefTheta;
Results.PrefK = PrefK;
Results.PrefPhi = PrefPhi;
Results.zThetaPref = zThetaPref;
Results.zThetaSel = zThetaSel;
Results.zPhiPref = zPhiPref;
Results.zPhiSel = zPhiSel;
Results.spatialCorrRF = spatialCorrRF;
Results.DThetaNeighbors = DThetaNeighbors;
Results.DThetaRandos = DThetaRandos;
Results.DPhiNeighbors = DPhiNeighbors;
Results.DPhiRandos = DPhiRandos;

[OSI, bipolarity, saturation] = OSIandBipolarity(Inp, Won, Woff, Results);
Results.OSI = OSI;
Results.bipolarity = bipolarity;
Results.saturation = saturation; 

Inp.Gs = Gs;
Inp.Gf = Gf;
Inp.A = A;

if d == 1
    figname = 'Feedback_1Dring';
elseif d ==2
    figname = 'MyFrame_2Dgrid';
end

% if ARBOR 
%     figname = [figname,'_Arbor'];
% end
if NORM 
    figname = [figname,'_Norm'];
end

switch fftpath 
    case 'yes'
    figname =[figname, '_fft'];
end
switch IntMethod 
    case 'Miller'
    figname = [figname, '_Miller'];
    case 'Euler'
    figname = [figname, '_Euler'];
end

if ONOFF
    figname = [figname,'_ONOFF'];
end

figname = [figname, num2str(ii)];

save(figname, 'Inp', 'Won','Woff','WRFon', 'WRFoff', 'Results', '-v7.3');
makePDFs(figname,Inp, WRFon, WRFoff, Results) %makes the standard pdf figure that I like.

% maps = Won*e1;
% salt = W*e2;
% neither = W*e3;


% saltRF = WRF*e2RF;
% neitherRF = WRF*e3RF;



% wp = zeros(nV1, (nsteps/10)+1, 3);
% wp = zeros([size(wp1), 3]);
% wp(:,:,1) = wp1;
% wp(:,:,2) = wp2;
% wp(:,:,3) = wp3;

%{
clear m s n
if d==1
    Inp.x = -L +(0:(nV1-1))*dx;
    m.Q = [mean(wp1.^2); mean(diff(wp1).^2)./mean(wp1.^2) ; mean(wp1)]; % the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
    s.Q = [mean(wp2.^2); mean(diff(wp2).^2)./mean(wp2.^2); mean(wp2)];
    n.Q = [mean(wp3.^2); mean(diff(wp3).^2)./mean(wp3.^2); mean(wp3)];
    
    m.QRF = [mean(wp1RF.^2); mean(diff(wp1RF).^2)./mean(wp1RF.^2); mean(wp1RF)]; % the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
    s.QRF = [mean(wp2RF.^2); mean(diff(wp2RF).^2)./mean(wp2RF.^2); mean(wp2RF)];
    n.QRF = [mean(wp3RF.^2); mean(diff(wp3RF).^2)./mean(wp3RF.^2); mean(wp3RF)];
    
elseif d==2
    % this finangling with wp's are so that q2 is the average difference
    % between elements in both directions. q2 needs to be the average
    % nearest neighbor difference in the vertical and horizontal. 
    m.Q(1,:) = mean(wp1.^2);
    s.Q(1,:) = mean(wp2.^2);
    n.Q(1,:)= mean(wp3.^2);
    m.Q(3,:) = mean(wp1);
    s.Q(3,:) = mean(wp2);
    n.Q(3,:) = mean(wp3);
    wp1 = reshape(wp1,nDim,nDim,[]);
    wp2 = reshape(wp2,nDim,nDim,[]);
    wp3 = reshape(wp2,nDim,nDim,[]);    
    wp1t = permute(wp1,[2,1,3]); %this is basically the transpose, and makes things work. 3rd dimension is time.
    wp2t = permute(wp2,[2,1,3]);    
    wp3t = permute(wp3,[2,1,3]);    
    m.Q(2,:) = (mean(squeeze(mean(diff(wp1).^2))) + mean(squeeze(mean(diff(wp1t).^2))))./m.Q(1,:)/2;% the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
    s.Q(2,:) = (mean(squeeze(mean(diff(wp2).^2))) + mean(squeeze(mean(diff(wp2t).^2))))./s.Q(1,:)/2;  
    n.Q(2,:) = (mean(squeeze(mean(diff(wp3).^2))) + mean(squeeze(mean(diff(wp3t).^2))))./n.Q(1,:)/2;
    
    m.QRF(1,:) = mean(wp1RF.^2);
    s.QRF(1,:) = mean(wp2RF.^2);
    n.QRF(1,:)= mean(wp3RF.^2);
    m.QRF(3,:) = mean(wp1RF);
    s.QRF(3,:) = mean(wp2RF);
    n.QRF(3,:) = mean(wp3RF);
    wp1RF = reshape(wp1RF,nDim,nDim,[]);
    wp2RF = reshape(wp2RF,nDim,nDim,[]);
    wp3RF = reshape(wp3RF,nDim,nDim,[]);    
    wp1tRF = permute(wp1RF,[2,1,3]); %this is basically the transpose, and makes things work. 3rd dimension is time.
    wp2tRF = permute(wp2RF,[2,1,3]);    
    wp3tRF = permute(wp3RF,[2,1,3]);    
    m.QRF(2,:) = (mean(squeeze(mean(diff(wp1RF).^2))) + mean(squeeze(mean(diff(wp1tRF).^2))))./m.QRF(1,:)/2;% the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
    s.QRF(2,:) = (mean(squeeze(mean(diff(wp2RF).^2))) + mean(squeeze(mean(diff(wp2tRF).^2))))./s.QRF(1,:)/2;  
    n.QRF(2,:) = (mean(squeeze(mean(diff(wp3RF).^2))) + mean(squeeze(mean(diff(wp3tRF).^2))))./n.QRF(1,:)/2;
end
%}
% H = MapsPlots(m, s, n, wp, growth, Inp,3);
