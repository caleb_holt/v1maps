% Arbor function

% function A = ArborMill(R, c, dist, D)
% dist comes from the neural distance function
% D is the length scale of my arbor function

%To find the overlap between two circles separated by a distance d, find
%the area of each circle segment minus the corresponding triangle that isn't in the overlap. 
%p is the base of triangle corresponding to radius R, s base of the
%triangle with radius c*R =r

r = c*R;

p = (dist.^2 - r^2 + R^2)./(2*dist); %distance from center of circle w/ radius R to line that is between the two segments
p(dist == 0) = min(R, r);
s = dist - p;
q = sqrt(R^2 - p.^2); %height of triangle (same for both circles).

thetap = 2*acos(p./R);
thetas = 2*acos(s./r);
areaCirc_R = (thetap./2).*R^2; %area of circle segment
areaCirc_r = (thetas./2).*r^2; %area of circle segment

areaTriang_R = p.*q; %normally Area(triangle) = 1/2 b*h but there are two equivalent triangles
areaTriang_r = s.*q;

areaOverlap = areaCirc_R - areaTriang_R + areaCirc_r - areaTriang_r;
areaOverlap(abs(dist) > R) = 0; %sets things outside the LGN circle of influence (AKA arbor) to zero

N = (pi*r^2)^2; %normalize the area overlap
A = areaOverlap./N