%neural distance calculator for various dimensions (d)

function [nDist, X] = neurDistance(nDim, d, L,PBC, spacing)
%d is the dimension, nDim the number of neurons per dimension, L = length of dimension;
%PBC = Periodic Boundary Conditions (1 = on, 0 = off)

nV1 = nDim^d;
% spacing = 2*L/(nV1);
if d == 1
    X = 1:nV1;
    X = -L + (X-1)*spacing; %now units are mm
    X = repmat(X, nV1, 1);
    if PBC
        X = bsxfun(@minus, X, X');
        X = abs(X);
        X(X > L) = 2*L - X(X > L);% -spacing;
        %X(X < -L) = -2*L - X(X < -L) + spacing;
        nDist = X.^2;
    else
        nDist = (bsxfun(@minus, X, X')).^2;
    end
    
elseif d==2
    [X, Y] = ind2sub([nDim, nDim], 1:nV1); %find the position (x,y) of each v1 neuron
%     round(sqrt(nV1)), round(sqrt(nV1)) 
    %X  = (X -1 -floor(nDim/2))*(2*L/nDim);
    % Y  = (Y - 1  - floor(nDim/2))*(2*L/nDim);
    X = -L + (X-1)*spacing;
    Y = -L + (Y-1)*spacing;
    X = repmat(X, nV1,1);
    Y = repmat(Y, nV1,1);
    X = bsxfun(@minus, X, X');
    Y = bsxfun(@minus, Y, Y');
    X = abs(X);
    Y = abs(Y);
    
    if PBC
        X(X > L) = 2*L - X(X > L);
        Y(Y > L) = 2*L - Y(Y > L);
    end
    
    nDist = X.^2 + Y.^2; %bsxfun(@minus, X, X').^2 + bsxfun(@minus, Y,Y').^2;
    nDist = sqrt(nDist);
    
%     nDist = (sqrt(nDist)*spacing).^2;
end