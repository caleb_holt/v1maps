function [m, s, n, wp,wpRF, W, Inp] = C2VisMaps(lambda2, af, ii)  %visualMaps(tau1, nDim) 
% [m, s, n, lTrue, growth,wp, W, Inp] = YasharVisMaps(10, 0.67, filenumber) --
% where filenumber is just used to save a file. 
% the third element of both lTrue and growth is the ratio of lTrue1/lTrue2,
% similar for growth.
%lambda2 multiplies the fast component of C(f). 
%af = alpha_f and multiplies the fast component of Gf. 
% 
% lambda2 = 10;
% af = 0.67;%0.3


SPARSE = 0; %determines if we use W as a sparse (SPARSE = 1) or not (SPARSE = 0) matrix
NORM = 1; %Doesn't normalize W (Norm=0), or uses Miller norm (NORM = 1);
ARBOR = 1; %uses the arbor function or doesn't (ARBOR = 0). 
ONOFF = 0; % whether to use ON and OFF cells for LGN (ONOFF = 0, don't use).
PBC = 1;%1 = impose periodic B.C.'s
IntMethod = 'Miller'; %My previous results were using the Euler method. 
fftpath = 'yes'; %Switch between fft ('yes') and not ('brute-force'). \

d = 1;%topographic/retinotopy dimension 

if d==1
    L = 10
    nDim = 1024;%256; %Miller 1994 uses 1024 neurons in a 32x32 grid
    R_arbor = 1; %sets length of arbor
   
elseif d==2
    L = 10%0.3125 %To make spacing the same for 1D case and 2D case
    nDim = 32; % want this to be 16
    R_arbor = 5*(2*L/nDim);% 4 * dx -- Gives me a radius of 5 nuerons in the arbor
end

nDimV1 = nDim;
nDimLGN = nDim;
spacing = 2*L/(nDim);
nV1 = nDim^d; %number of V1 neurons -- one dimensional currently
nThal = nV1; %sets size of input layer. =nV1 means equal number of input and output neurons

eps = 0; %sets scale of epsilon. Set eps=0 to not include order epsilon terms in dW/dt

aE = 10;
aI = 10;

sigE = 0.5; %These are the Gs sigmas
sigI = 3*sigE;
sigf = 0.01; % this is the Gf sigma

sE1 = 0.3; % defines the correlations sigmas -- I think these are the problem.
sI1 = 10;
sE2 = 0.1/3; % got this from Analyzing things in CorrEig;
sI2 = 0.3/3;% divide by 3 so the fast correlation oscillates more


tau1 = 100;
tau2 = 1;
gs = 1/tau1; %this is gamma_s from Yashar's notes -- arbitrarily setting gamma_s = 1/tau1
gf = 1/tau2; % this is gamma_f from Yashar's notes

lambda1 = 1; %lambda2 is given as first input to function. (normally 10);


dt = 0.1*tau2; % dt sets the time scale -- currently 1/10 of tau2.
tauW = 1000;
scaleTauW = tauW/(lambda1*aE);

%=========================================================================
Inp.L = L;
Inp.d = d;
Inp.nDim = nDim;
Inp.nDimV1 = nDimV1;
Inp.nDimLGN = nDimLGN;
Inp.sigE = sigE;
Inp.sigI = sigI;
Inp.sE1 = sE1;
Inp.sI1 = sI1;
Inp.sE2 = sE2;
Inp.sI2 = sI2;
Inp.aE = aE;
Inp.aI = aI;
Inp.spacing = spacing;
Inp.PBC = PBC; %determines if we're using Periodic Boundary conditions (PBC = 1) or not (PBC = 0)
Inp.Norm = NORM;
Inp.eps = eps;
Inp.nV1 = nV1;
Inp.nThal = nThal;%floor(nV1/2);
Inp.lambda1 = lambda1; 
Inp.lambda2 = lambda2;
Inp.tau = [tau1; tau2; tauW]; %tau1, tau2, tauW defined in ConDefinitions.mat
Inp.scaleTau = tauW/(lambda1*aE);
Inp.fftpath = fftpath;
Inp.IntMethod = IntMethod;

%=========================================================================
%get distances on the grid (in d==1, distV1 gives distances between all pairs of grid-points, in d==2, it gives distances of all grid-points with grid-point at (-L,-L):
[distV1sq, Xdist] = neurDistance2(nDim, d, L,PBC);
distV1 = sqrt(distV1sq);
%--------------------------------------------------------
%make arbor function
if ARBOR 
	[A,~,~,RFinds,Nx_arbor] = Arbor2(Xdist, R_arbor,d, nDim,nDim,L); %R_arbor = radius of the arbor
elseif ~ARBOR
    A = ones(nV1,nThal);
    Nx_arbor = nThal;
    RFinds = (1:nV1*nThal)';
end

if ONOFF
    A = [A,A];
end


nRF = sum(A(1,:));%number of presynaptic inputs to a V1 cell, also W is initialized and normalized such that sum(W,2) stays equal to nRF

%--------------------------------------------------------
%make p's or C's
% currently assuming same grid (resolution) in LGN as in V1 
if d==1
    if ARBOR
        Inp.stc = 'corr1';
        p1_pars.sigEI = [sE1,sI1];
        p1_pars.aEI = [aE,aI];
        p2_pars.sigEI = [sE2, sI2];%0.5 * p1_pars.sigEI;
        p2_pars.aEI = p1_pars.aEI;
        [p1, p2, e1,e2, e3] = CorrProj(distV1, nThal, Inp.stc, sE1, sI1, sE2, sI2, aE, aI, 1);%3,5,spacing/5, spacing,  sigE, sigI, 0.5*sigE, 0.5*sigI,
        
       
        [e1RF,e2RF,e3RF] =  RFcovPCs(spacing,d,p1_pars,p2_pars, R_arbor); %RF-sized vectors
        
    else
        Inp.stc = 'proj';
        [p1, p2, e1,e2, e3] = CorrProj(distV1, nThal, Inp.stc, sigE, sigI, 0.5*sigE, 0.5*sigI, aE, aI, 1);%3,5,spacing/5, spacing
    end
elseif d==2 
    if ~ONOFF
        Inp.stc = 'corr2D';
        p1_pars.sigEI = [sE1, sI1];%[1,1];
        p1_pars.aEI = [aE, aI];
        p2_pars.sigEI = [sE2, sI2];%0.5 *[1,1];
        p2_pars.aEI = [aE,aI];
        [p1, p2,e1,e2,e3] = CorrProj2D(distV1, p1_pars,p2_pars, R_arbor);%3,5,spacing/5, spacing
        [e1RF,e2RF,e3RF] =  RFcovPCs(spacing,d,p1_pars,p2_pars, R_arbor);
    else
        Inp.stc = 'corr2D_ONOFF';
        p1same_pars.sigEI = [1,1];
        p1same_pars.aEI = [1,1];
        p2same_pars.sigEI = 0.5 *[1,1];
        p2same_pars.aEI = [1,1];
        [p1_same, p2_same] = CorrProj2D(distV1, p1same_pars,p2same_pars, R_arbor);%3,5,spacing/5, spacing
        [p1_diff, p2_diff] = CorrProj2D(distV1, p1diff_pars,p2diff_pars, R_arbor);%3,5,spacing/5, spacing
        [e1,e2,e3] =  RFcovPCs(spacing,d,sigE*[1,0.5], sigI*[1,0.5], aE*[1,1], aI*[1,1], R_arbor);
    end
end

Inp.A = A;
Inp.R_arbor = R_arbor;
Inp.nRF = nRF;
Inp.Nx_arbor = Nx_arbor;
Inp.RFinds = RFinds;
Inp.p1 = p1;
Inp.p2 = p2;
Inp.e1 = e1;
Inp.e2 = e2;
Inp.e3 = e3;
Inp.e1RF = e1RF;
Inp.e2RF = e2RF;
Inp.e3RF = e3RF;
Inp.endCondition = 0.1*nRF*nV1;
%--------------------------------------------------------
%make G's 
Gs = makeMexHat2(distV1,sigE,aE,sigI,aI,d);
Gf = makeMexHat2(distV1,sigf,af,0,0,d);

Inp.Gs = Gs;
Inp.Gf = Gf;

G1 = 0.5* Gs + tau1/(tau1+tau2)* Gf;
G2 = tau2/(tau2+tau1)* Gs + 0.5* Gf;

% CorrPlots(nDim, A, p1, p2, G1, G2)
Inp.G1 = G1;
Inp.G2 = G2;
%--------------------------------------------------------
%make the pure (unnormalized/unconstrained) dWdt 
switch Inp.stc
    case {'proj','corr1'}
        switch fftpath
            case 'brute-force'
                dWdt = @(W) 1/4 * A.*( lambda1*G1*W*p1 + lambda2*G2*W*p2 + eps*(gs*Gs + gf*Gf)*W ); %this adds a more complete term. ;
                lambda1fft = max(real(lambda1*fft(G1(1,:)')))
                lambda2fft = max(real(lambda2*fft(G2(1,:)')))
            case 'yes' %currently this case assumes eps is 0
                if d~=1
                    error('d must be 1, in case corr1');
                end
                GC1fft = fft(G1(1,:)') * fft(p1(1,:)); %nV1 x nThal matrix
                GC2fft = fft(G2(1,:)') * fft(p2(1,:)); %nV1 x nThal matrix
                dWdt = @(W) 1/4 * fastdWdt(W,A, lambda1*GC1fft + lambda2*GC2fft);
                lambda1fft = max(real(lambda1*GC1fft(:)))
                lambda2fft = max(real(lambda2*GC2fft(:)))
        end
    case 'corr2D' %currently this case assumes eps is 0
        switch fftpath
            case 'yes'
                G1fft = reshape(G1(:,1), nDimV1, nDimV1); %gets first matrix describing connectivity between (-L, -L) neuron and rest of network in V1
                G1fft = reshape(fft2(G1fft), nV1,1); %takes the fft in 2d and then vectorizes the results
                C1fft = reshape(p1(:,1), nDimLGN, nDimLGN); %gets first matrix describing connectivity between (-L, -L) neuron and the rest of network in Thalamus
                C1fft = reshape(fft2(C1fft),nThal, 1); %takes the fft in 2d and then vectorizes the results
                GC1fft = kron(C1fft, G1fft); %takes the (dirac? tensor?) product of the two. So now dimensionality is (nV1*nThal)x1
                lambda1fft = max(real(lambda1*GC1fft))
                GC1fft = reshape(GC1fft,[nDimV1,nDimV1,nDimLGN,nDimLGN]); %think of this as like 2 2-vector indices. In the 1D case, those 2-vectors become 1-vectors. 
                
                G2fft = reshape(G2(:,1), nDimV1, nDimV1); %gets first matrix describing connectivity between (-L, -L) neuron and rest of network in V1
                G2fft = reshape(fft2(G2fft), nV1,1); %takes the fft in 2d and then vectorizes the results
                C2fft = reshape(p2(:,1), nDimLGN, nDimLGN); %gets first matrix describing connectivity between (-L, -L) neuron and the rest of network in Thalamus
                C2fft = reshape(fft2(C2fft),nThal, 1); %takes the fft in 2d and then vectorizes the results
                GC2fft = kron(C2fft, G2fft); %takes the (dirac? tensor?) product of the two. So now dimensionality is (nV1*nThal)x1
                lambda2fft = max(real(lambda2*GC2fft))
                GC2fft = reshape(GC2fft,[nDimV1,nDimV1,nDimLGN,nDimLGN]); %think of this as like 2 2-vector indices. In the 1D case, those 2-vectors become 1-vectors. 
%{                
                G1fft = fft2(reshape(G1, nDimV1, nDimV1, nDimV1, nDimV1));
                G1fft = reshape(G1fft, nV1, nV1);%Cause I could reshape G1 into a nDim x nDim x nDim x nDim and each nDim x nDim should have the same real fft2. __ %reshape(fft2(G1),nV1,1);%over V1 vectorized freq-grid
                C1fft = fft2(reshape(p1, nDimLGN, nDimLGN,nDimLGN,nDimLGN));
                C1fft = reshape(C1fft, nThal, nThal);%reshape(fft2(p1),nThal,1);%over LGN vectorized freq-grid
                GC1fft = G1fft*C1fft;%kron(C1fft,G1fft);%V1 freq's for constant LGN-freq are consecutive --appropriate for W which is V1 x LGN
                lambda1fft = max(max(real(lambda1*GC1fft)));
%                 GC1fft = reshape(GC1fft,[nDimV1,nDimV1,nDimLGN,nDimLGN]);

                G2fft = fft2(reshape(G2, nDimV1, nDimV1, nDimV1, nDimV1));
                G2fft = reshape(G2fft, nV1, nV1);%Cause I could reshape G1 into a nDim x nDim x nDim x nDim and each nDim x nDim should have the same real fft2. __ %reshape(fft2(G1),nV1,1);%over V1 vectorized freq-grid
                C2fft = fft2(reshape(p2, nDimLGN, nDimLGN,nDimLGN,nDimLGN));
                C2fft = reshape(C2fft, nThal, nThal);%reshape(fft2(p1),nThal,1);%over LGN vectorized freq-grid
                GC2fft = G2fft*C2fft;%kron(C1fft,G1fft);%V1 freq's for constant LGN-freq are consecutive --appropriate for W which is V1 x LGN
                lambda2fft = max(max(real(lambda2*GC2fft)));
%}
%                 G2fft = fft2(G2);%reshape(fft2(G2),nV1,1);
%                 C2fft = fft2(p2);%reshape(fft2(p2),nThal,1);
%                 GC2fft = G2fft*C2fft; %kron(C2fft,G2fft);%V1 freq's for constant LGN-freq are consecutive --appropriate for W which is V1 x LGN
%                 lambda2fft = max(real(lambda2*GC2fft))
%                 GC2fft = reshape(GC2fft,[nDimV1,nDimV1,nDimLGN,nDimLGN]);
                dWdt = @(W) 1/4 * fastdWdt2(W,A, lambda1*GC1fft + lambda2*GC2fft, nDimV1, nDimLGN);%,nDimV1,nDimLGN);
            case 'brute-force'
                dWdt = @(W) 1/4 * A.*( lambda1*G1*W*p1 + lambda2*G2*W*p2 + eps*(gs*Gs + gf*Gf)*W );
                lambda1fft = max(max(real(lambda1*fft2(G1))));
                lambda2fft = max(max(real(lambda2*fft2(G2))));
        end
end

Inp.lambda1fft = lambda1fft;
Inp.lambda2fft = lambda2fft;
%--------------------------------------------------------
% set the stopping time


 dt = 0.1;
% tauW = 100;

ts = 5*10*tauW/(lambda1*aE); % to run for long enough time so that dynamics are well-developped.
ts = 100*ts;
nsteps = round(ts/dt)

% nsteps = 200

Inp.totalTime = ts;
Inp.Nsteps = nsteps;


%--------------------------------------------------------


%--------------------------------------------------------
%set the initial condition for W
Wmax = 4;
Wmean = 1;
Wnoise = 0.2;
if ~SPARSE
    W = Wnoise*(2*rand(nV1, nThal)-1) + Wmean;%Uniform distribution on [Wmean-Wnoise, Wmean+Wnoise];
    W = A.*W;
    if NORM %rescale to set the sum of weights to V1 cells equal to sum(A,2)
        W = repmat(sum(A,2)./sum(W,2) ,1, nThal).*  W ; 
    end
elseif SPARSE
    sparsity = 0.1;
    W = rand(nV1, nThal);
    W(W > sparsity) = 0;
    W(W ~= 0) = Wmax*rand;
    nIn = sum(W ~=0 ,2);
    W = nIn.*W./repmat(sum(W,2),1,nThal);
end

Inp.W = W;
Inp.Wmax = Wmax;
%--------------------------------------------------------

% wp1 = zeros(nV1, (nsteps/100)+1); % I don't save every times step so wp's stay smaller in size
% wp2 = wp1;
% wp3 = wp1;
% wp1RF = wp1;
% wp2RF = wp1;
% wp3RF = wp1;
% ts_wp = wp1(1,:);
% 
% 
% WRF = W';%when vectorized LGN-indices for the same V1-cell are consecutive now
% WRF = reshape(WRF(RFinds),[Nx_arbor^d,nV1])';%get square-RF's of all V1 cells.
% 
% wp1RF(:,1) = WRF*e1RF;%"selectivity"/overlap of V1 cell RF's for/with e1
% wp2RF(:,1) = WRF*e2RF;%"selectivity"/overlap of V1 cell RF's for/with e2
% wp3RF(:,1) = WRF*e3RF;%"selectivity"/overlap of V1 cell RF's for/with e2
% wpRF(:,:,1) = wp1RF;
% wpRF(:,:,2) = wp2RF;
% wpRF(:,:,3) = wp3RF;
% 
% wp1(:,1) = W*e1;
% wp2(:,1) = W*e2;
% wp3(:,1) = W*e3;
% wp(:,:,1) = wp1;
% wp(:,:,2) = wp2;
% wp(:,:,3) = wp3;

        

%========================================================================

%run Hebbian learning

if NORM
    %     while sum(sum(or(W >= Wmax, W <= 0),2)) ~= nV1*nThal % could do it
    %     this way to make all synapses inactive or
    switch IntMethod
        case 'Miller'
            [W, WRF, wp1, wp2, wp3,wp1RF, wp2RF, wp3RF, ts_wp, nsteps, timeUp,dt] = MillerInt(W, dWdt, Inp);
            wp(:,:,1) = wp1;
            wp(:,:,2) = wp2;
            wp(:,:,3) = wp3;
            wpRF(:,:,1) = wp1RF;
            wpRF(:,:,2) = wp2RF;
            wpRF(:,:,3) = wp3RF;
            Inp.Nsteps = nsteps;
            Inp.dt = dt;
            
        case 'Euler'
            [W, wp, wpRF, ts_wp, timeUp] =  EulerMethod(W, dWdt, Inp, dt,ts);
            wp1 = wp(:,:,1);
            wp2 = wp(:,:,2);
            wp3 = wp(:,:,3);
            wp1RF = wpRF(:,:,1);
            wp2RF = wpRF(:,:,2);
            wp3RF = wpRF(:,:,3);
            Inp.dt = dt;
            
    end
Inp.endT = timeUp;

elseif ~NORM
    for t = 1:nsteps
        W = W +  (dt/tauW)*dWdt(W);        
        
        if mod(t,10) == 0
            %      wtracker(:,:,t+1) = W;
            WRF = W';
            WRF = reshape(WRF(RFinds),[Nx_arbor^d,nV1])';
            wp1(:,(t/10)+1) = WRF*e1;
            wp2(:,(t/10)+1) = WRF*e2;
            wp3(:,(t/10)+1) = WRF*e3;
            ts_wp((t/10)+1) = t*dt;
        end
    end
Inp.endT = t;    
end


%========================================================================


WRF = W';
WRF = reshape(WRF(RFinds),[Nx_arbor^d,nV1])';%size(WRF) = [nV1,Nx_arbor^d]
if d==2    
    %WRF = reshape(WRF,nV1,Nx_arbor,Nx_arbor);%all RF's
    WRFplot = reshape(WRF,nDimV1,nDimV1,Nx_arbor,Nx_arbor);%all RF's
    WRFplot = permute(WRFplot, [3,1,4,2]);
    WRFplot = reshape(WRFplot, [nDimV1*Nx_arbor , nDimV1*Nx_arbor]);
    
end            
maps = W*e1;
salt = W*e2;
neither = W*e3;

mapsRF = WRF*e1RF;
saltRF = WRF*e2RF;
neitherRF = WRF*e3RF;



% wp = zeros(nV1, (nsteps/10)+1, 3);
% wp = zeros([size(wp1), 3]);
% wp(:,:,1) = wp1;
% wp(:,:,2) = wp2;
% wp(:,:,3) = wp3;

clear m s n
if d==1
    Inp.x = -L +(0:(nV1-1))*spacing;
    m.Q = [mean(wp1.^2); mean(diff(wp1).^2)./mean(wp1.^2) ; mean(wp1)]; % the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
    s.Q = [mean(wp2.^2); mean(diff(wp2).^2)./mean(wp2.^2); mean(wp2)];
    n.Q = [mean(wp3.^2); mean(diff(wp3).^2)./mean(wp3.^2); mean(wp3)];
    
    m.QRF = [mean(wp1RF.^2); mean(diff(wp1RF).^2)./mean(wp1RF.^2); mean(wp1RF)]; % the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
    s.QRF = [mean(wp2RF.^2); mean(diff(wp2RF).^2)./mean(wp2RF.^2); mean(wp2RF)];
    n.QRF = [mean(wp3RF.^2); mean(diff(wp3RF).^2)./mean(wp3RF.^2); mean(wp3RF)];
    
elseif d==2
    % this finangling with wp's are so that q2 is the average difference
    % between elements in both directions. q2 needs to be the average
    % nearest neighbor difference in the vertical and horizontal. 
    m.Q(1,:) = mean(wp1.^2);
    s.Q(1,:) = mean(wp2.^2);
    n.Q(1,:)= mean(wp3.^2);
    m.Q(3,:) = mean(wp1);
    s.Q(3,:) = mean(wp2);
    n.Q(3,:) = mean(wp3);
    wp1 = reshape(wp1,nDim,nDim,[]);
    wp2 = reshape(wp2,nDim,nDim,[]);
    wp3 = reshape(wp2,nDim,nDim,[]);    
    wp1t = permute(wp1,[2,1,3]); %this is basically the transpose, and makes things work. 3rd dimension is time.
    wp2t = permute(wp2,[2,1,3]);    
    wp3t = permute(wp3,[2,1,3]);    
    m.Q(2,:) = (mean(squeeze(mean(diff(wp1).^2))) + mean(squeeze(mean(diff(wp1t).^2))))./m.Q(1,:)/2;% the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
    s.Q(2,:) = (mean(squeeze(mean(diff(wp2).^2))) + mean(squeeze(mean(diff(wp2t).^2))))./s.Q(1,:)/2;  
    n.Q(2,:) = (mean(squeeze(mean(diff(wp3).^2))) + mean(squeeze(mean(diff(wp3t).^2))))./n.Q(1,:)/2;
    
    m.QRF(1,:) = mean(wp1RF.^2);
    s.QRF(1,:) = mean(wp2RF.^2);
    n.QRF(1,:)= mean(wp3RF.^2);
    m.QRF(3,:) = mean(wp1RF);
    s.QRF(3,:) = mean(wp2RF);
    n.QRF(3,:) = mean(wp3RF);
    wp1RF = reshape(wp1RF,nDim,nDim,[]);
    wp2RF = reshape(wp2RF,nDim,nDim,[]);
    wp3RF = reshape(wp3RF,nDim,nDim,[]);    
    wp1tRF = permute(wp1RF,[2,1,3]); %this is basically the transpose, and makes things work. 3rd dimension is time.
    wp2tRF = permute(wp2RF,[2,1,3]);    
    wp3tRF = permute(wp3RF,[2,1,3]);    
    m.QRF(2,:) = (mean(squeeze(mean(diff(wp1RF).^2))) + mean(squeeze(mean(diff(wp1tRF).^2))))./m.QRF(1,:)/2;% the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
    s.QRF(2,:) = (mean(squeeze(mean(diff(wp2RF).^2))) + mean(squeeze(mean(diff(wp2tRF).^2))))./s.QRF(1,:)/2;  
    n.QRF(2,:) = (mean(squeeze(mean(diff(wp3RF).^2))) + mean(squeeze(mean(diff(wp3tRF).^2))))./n.QRF(1,:)/2;
end

Inp.Gs = Gs;
Inp.Gf = Gf;
Inp.A = A;

% H = MapsPlots(m, s, n, wp, growth, Inp,3);


if d == 1
    figname = 'V1Maps_1Dring';
elseif d ==2
    figname = 'V1Maps_2Dgrid';
end

% if ARBOR 
%     figname = [figname,'_Arbor'];
% end
if NORM 
    figname = [figname,'_Norm'];
end

switch fftpath 
    case 'yes'
    figname =[figname, '_fft'];
end
switch IntMethod 
    case 'Miller'
    figname = [figname, '_Miller'];
    case 'Euler'
    figname = [figname, '_Euler'];
end

if ONOFF
    figname = [figname,'_ONOFF'];
end

figname = [figname, num2str(ii)];

save(figname, 'Inp', 'm', 's', 'n', 'wp', 'wpRF','W','WRF');


figure(22);clf;
if d==1
    imagesc(WRF);
elseif d==2
%     colormap('gray');
    imagesc(WRFplot);
    colormap;
    colorbar;
end


figure(23);clf;
subplot 131;
plot(ts_wp,m.Q(1,:),'LineWidth',2);
title('Q1');
hold on;
plot(ts_wp(1:(timeUp+1)),s.Q(1,1:(timeUp+1)),'LineWidth',2);
plot(ts_wp(1:(timeUp+1)),n.Q(1,1:(timeUp+1)),'LineWidth',2);
hold off;

subplot 132;
plot(ts_wp(1:(timeUp+1)),m.Q(2,1:(timeUp+1)),'LineWidth',2);
title('Q2');
hold on;
plot(ts_wp(1:(timeUp+1)),s.Q(2,1:(timeUp+1)),'LineWidth',2);
plot(ts_wp(1:(timeUp+1)),n.Q(2,1:(timeUp+1)),'LineWidth',2);
hold off;

subplot 133;
plot(ts_wp(1:(timeUp+1)),m.Q(3,1:(timeUp+1)),'LineWidth',2);
title('Q3');
hold on;
plot(ts_wp(1:(timeUp+1)),s.Q(3,1:(timeUp+1)),'LineWidth',2);
plot(ts_wp(1:(timeUp+1)),n.Q(3,1:(timeUp+1)),'LineWidth',2);
hold off;


if d==1
    figure(24);clf;
    subplot 131;
    plot(Inp.x',wp1(:,timeUp+1));
    title('slow map');

    subplot 132;
    plot(Inp.x',wp2(:,timeUp+1));
    title('fast map');

    subplot 133;
    plot(Inp.x',wp3(:,timeUp+1));
    title('neither map');

    figure(25);clf;
    subplot 131;
    plot(Inp.x',wp1RF(:,timeUp+1));
    title('slow map');

    subplot 132;
    plot(Inp.x',wp2RF(:,timeUp+1));
    title('fast map');

    subplot 133;
    plot(Inp.x',wp3RF(:,timeUp+1));
    title('neither map');

end

figure(13); clf;
subplot(4, 2, 1)
imagesc(W)
colorbar
title('W')

subplot(4, 2, 2)
imagesc(WRF)
colorbar
title('Receptive Fields')

subplot(4, 2, 3)
plot(Inp.x', maps, 'LineWidth', 1.25);
title('Projection of W on slow vector')
xlabel('Neuron Position')
ylabel('Proj(W)')
hold on
plot(Inp.x', max(maps)*Gs(round(Inp.nV1/2),:)+mean(maps), 'r')
plot([-8; -8+sigE], [mean(maps); mean(maps)], '-k',[2; 2+sigI],[mean(maps); mean(maps)],'-r',[0; sigI-sigE],[mean(maps); mean(maps)],'y', 'LineWidth',4)
hold off

subplot(4,2,4)
plot(ts_wp, m.Q(1,:), ts_wp, s.Q(1,:), ts_wp, n.Q(1,:),'LineWidth',2)
ylabel('q1')
xlabel('Time')
title('Squared Mean')

subplot(4,2,5)
plot(Inp.x', salt, 'LineWidth', 1.25)
title('Projection of W on fast vector')
xlabel('Neuron Position')
ylabel('Proj(W)')

subplot(4,2,6)
plot(ts_wp, m.Q(2,:), ts_wp, s.Q(2,:), ts_wp, n.Q(2,:),'LineWidth',2)
ylabel('q2')
xlabel('Time')
title('Nearest Neighbor Distance')

subplot(4,2,7)
plot(Inp.x', neither, 'LineWidth', 1.25)
title('Projection of W on Neither')
xlabel('Neuron Position')
ylabel('Proj(W)')

subplot(4,2,8)
plot(ts_wp, m.Q(3,:), ts_wp, s.Q(3,:), ts_wp, n.Q(3,:),'LineWidth',2)
ylabel('q3')
xlabel('Time')
title('Mean')
legend('Slow Vector (Maps)', 'Fast Vector (Salt)', 'Neither')