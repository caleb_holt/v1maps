% Simulate a network of neurons and see how 'maps' develop using complex
% synapses
%Takes tau1 as its input function. Tau2 is set to 1. TauW = 100. All these
%are in indeterminate units of 'time steps'. 
function [m, s, n, lTrue, growth,wp, W, Inp] = arborlessMaps(lambda2, af, varargin)  %visualMaps(tau1, nDim) 

% the third element of both lTrue and growth is the ratio of lTrue1/lTrue2,
% similar for growth.

if nargin > 2
    ii = varargin{1};
end

load('ConDefinitions.mat'); % file that holds constants values. to be called in multiple functions
Inp.sigE = sigE;
Inp.sigI = sigI;
Inp.aE = aE;
Inp.aI = aI;
Inp.spacing = spacing;
Inp.PBC = PBC; %determines if we're using Periodic Boundary conditions (PBC = 1) or not (PBC = 0)
Inp.distV1 = distV1;

SPARSE = 0; %determines if we use W as a sparse (SPARSE = 1) or not (SPARSE = 0) matrix
SIMPLE = 0; % 1 - step fcn approx, 2 - integral form, 3 - analytic solution
NORM = 1; %Doesn't normalize W (Norm=0), or uses Miller norm (NORM = 1);


Inp.Norm = NORM;
eps = 0.1; %sets scale of epsilon. Set eps=0 to not include order epsilon terms in dW/dt
Inp.eps = eps;

nV1 = nDim^d; %number of V1 neurons -- one dimensional currently
Inp.nV1 = nV1;
dt = 0.1*tau2; % dt sets the time scale -- currently 1/10 of tau2.

nThal = nV1; %sets size of input layer. =nV1 means equal number of input and output neurons
Inp.nThal = nThal;%floor(nV1/2);

% %make projection operators by Graham-Schmidt Orthonormalization
% e1 = randn(nThal, 1);
% e1 = e1/sqrt(e1'*e1);
% p1 = e1*e1';
% e2 = randn(nThal, 1);
% e2 = e2 - (e1'*e2)*e1;
% e2 = e2/sqrt(e2'*e2);
% p2 = e2*e2';
% e3 = randn(nThal,1);
% e3 = e3 - (e2'*e3)*e2 - (e1'*e3)*e1;
% e3 = e3/sqrt(e3'*e3);
[p1, p2, e1, e2, e3] = CorrProj(distV1, nThal, 'proj', sigE, sigI, 0.5*sigE, 0.5*sigI);%3,5,spacing/5, spacing

Inp.e1 = e1;
Inp.e2 = e2;
Inp.e3 = e3;

lambda1 = 1; %lambda2 is given as first input to function.
Inp.lambda1 = lambda1; 
Inp.tau = [tau1; tau2; tauW]; %tau1, tau2, tauW defined in ConDefinitions.mat
Inp.scaleTau = tauW/(lambda1*aE);
if NORM
    ts = 5*10*tauW/(lambda1*aE); % to run for long enough time so that dynamics are well-developped.
    Inp.totalTime = ts;
    
    nsteps = round(ts/dt);
    Inp.Nsteps = nsteps;
    
    wp1 = zeros(nV1, (nsteps/10)+1); % I don't save every times step so wp's stay smaller in size
    wp2 = zeros(nV1, (nsteps/10)+1);
    wp3 = wp1;
    wp = zeros(nV1, (nsteps/10)+1, 3);
else
    ts = 5*10*tauW/(lambda1*aE); % to run for long enough time so that dynamics are well-developped.
    Inp.totalTime = ts;
    
    nsteps = round(ts/dt);
    Inp.Nsteps = nsteps;
    
    wp1 = zeros(nV1, (nsteps/10)+1); % I don't save every times step so wp's stay smaller in size
    wp2 = zeros(nV1, (nsteps/10)+1);
    wp3 = wp1;
    wp = zeros(nV1, (nsteps/10)+1, 3);
end

%define G's 
G_E = exp(-distV1./(2*(sigE).^2));
G_E = G_E/sum(G_E(1,:)); %normalizes G_E
G_I = exp(-distV1./(2*(sigI).^2));
G_I = G_I/sum(G_I(1,:)); %normalizes G_I
Gs = aE*G_E - aI*G_I; %combines the two into a Mexican Hat
Gf = exp(-distV1./(2*(sigf).^2));
Gf = Gf/sum(Gf(1,:)); %%normalizes Gf
Gf = af*Gf;

Inp.Gs = Gs;
Inp.Gf = Gf;

[V1, eigA1] = eig((lambda1/4)*(0.5*Gs + 1/(1+ (tau2/tau1))*Gf), 'vector');
[lTrue1, ind1] = max(real(eigA1));

[V2, eigA2] = eig((lambda2/4)*(1/(1+(tau1/tau2))*Gs + 0.5*Gf), 'vector');
[lTrue2, ind2] = max(real(eigA2));
lTrue = [lTrue1; lTrue2; lTrue1/lTrue2];

gs = 1/tau1; %this is gamma_s from Yashar's notes -- arbitrarily setting gamma_s = 1/tau1
gf = 1/tau2; % this is gamma_f from Yashar's notes

if SIMPLE ==1 % approximation using step functions
    dWdt = @(W) lambda1*(Gs + Gf)*(W*p1) + lambda2*((tau2/tau1)*Gs + Gf)*W*p2;
elseif SIMPLE == 2 % this is the integral form 
    KC = @(f, W) (Gs.*(abs(1 + 2*pi*1i*f/gs)).^(-2) + Gf.*(abs(1 + 2*pi*1i*f/gf)).^(-2))*W*...
        (lambda1*tau1*(abs(1 + 2*pi*1i*f*tau1).^(-2)).*p1 + lambda2*tau2*(abs(1 + 2*pi*1i*f*tau2).^(-2)).*p2);
    %KC multiplies the real part of K with C(f) for ease of use with integral 
    dWdt = @(W) integral(@(f) KC(f,W), 0, inf, 'ArrayValued', true);
elseif SIMPLE == 0 % analytic solution to the integral I derived. 
    dWdt = @(W) (lambda1/4)*(0.5*Gs + 1/(1+ (tau2/tau1))*Gf)*W*p1 + (lambda2/4)*(1/(1+(tau1/tau2))*Gs + 0.5*Gf)*W*p2+(eps/4)*(gs*Gs + gf*Gf)*W; %this adds a more complete term. ;     
end

Wmax = 4;
if ~SPARSE
    W = (Wmax/4)*rand(nV1, nThal);%(Wmax/4) is so we don't start close to the upper bound
%     W = A.*W;
    if NORM
        W = repmat(sum(A,2),1, nThal).*W./repmat(sum(W,2),1,nThal); % nThal*W./repmat(sum(W,2),1,nThal);
        Wact = zeros(nV1,1);
        Wfroz = Wact;
    end
    
elseif SPARSE
    sparsity = 0.1;
    W = rand(nV1, nThal);
    W(W > sparsity) = 0;
    W(W ~= 0) = Wmax*rand;
    nIn = sum(W ~=0 ,2);
    W = nIn.*W./repmat(sum(W,2),1,nThal);
end
% logicW = or(W >= Wmax, W <= 0);
% dW = zeros(nV1, nThal); % apparently doesn't need to be pre-allocated 
% wtracker = zeros([size(W), nsteps+1]);
% wtracker(:,:, 1) = W;
% wp1 = zeros(nV1, (nsteps/1000)+1); % I don't save every times step so wp's stay smaller in size
% wp2 = zeros(nV1, (nsteps/1000)+1);
% wp3 = wp1;
% % q1 = zeros(3, (nsteps/10)+1); % also true with q's
% wp = zeros(nV1, (nsteps/1000)+1, 3);
% wp1 = zeros(nV1, nsteps+1);
% wp2 = zeros(nV1, nsteps+1);
% wp3 = wp1;
% q1 = zeros(3, nsteps+1);

wp1(:,1) = W*e1;
wp2(:,1) = W*e2;
wp3(:,1) = W*e3;

% growth1 = exp(lTrue1)*(V1(:,ind1)'*wp1(:,1));
growth1 = (V1(:,ind1)'*wp1(:,1));
% growth2 = exp(lTrue2)*(V2(:,ind2)'*wp2(:,1));
growth2 = (V2(:,ind2)'*wp2(:,1));
growth = [growth1; growth2; growth1/growth2];

% q1(1,1) = sum(W*e1).^2;
% q1(2,1) = sum(W*e2).^2;
% q1(3,1) = sum(W*e3).^2;

if NORM
%     while sum(sum(or(W >= Wmax, W <= 0),2)) ~= nV1*nThal % could do it
%     this way to make all synapses inactive or
    for t =1:nsteps
        dW = (1/tauW)*dWdt(W);
        
        dW(W >= Wmax) = 0;
        dW(W <= 0) = 0;
        nAct = sum(and(W < Wmax, W >0),2);
        if sum(nAct) == 0
            Inp.endT = t;
            break
        end 
        epsilon = sum(dW,2)./nAct; %epsilon will constrain the growth of W. 
        epsilon = repmat(epsilon, 1, nThal);
        epsilon(or(W >= Wmax, W <=0)) = 0;
        epsilon = epsilon; %from eq 2- Miller 1994 paper. It's the arbor function times the contraining epsilon. 
        dW = dW - epsilon;
        W1 = dW*dt + W;
        W1(W1 > Wmax) = Wmax;
        W1(W1 < 0) = 0;
        
        Wact = sum(W1.*and(W1 < Wmax, W1 > 0),2);
        Wfroz = sum(W1.*or(W1 >=Wmax, W1<= 0),2);
        
        
%         for cc = 1:nV1
%             Wact(cc) = sum(W1(cc, dW(cc,:)~=0));
%             Wfroz(cc) = sum(W1(cc, dW(cc,:)==0));
%         end
       
        gamma = (-Wfroz + nThal)./Wact;
        gamma = repmat(gamma, 1, nThal);
        
        
        gamma(or(W1 >= Wmax, W1<=0)) = 1;
        W = W1.*gamma;
        
        if mod(t,10) == 0
            %      wtracker(:,:,t+1) = W;
            wp1(:,(t/10)+1) = W*e1;
            wp2(:,(t/10)+1) = W*e2;
            wp3(:,(t/10)+1) = W*e3;
            
%             q1(1,(t/10)+1) = sum(W*e1).^2;
%             q1(2,(t/10)+1) = sum(W*e2).^2;
%             q1(3,(t/10)+1) = sum(W*e3).^2;
        end
    end
    
elseif ~NORM
    for t = 1:nsteps
        dW = (1/tauW)*dWdt(W);
        
        W = dW*dt + W;
        
        if mod(t,10) == 0
            %      wtracker(:,:,t+1) = W;
            wp1(:,(t/10)+1) = W*e1;
            wp2(:,(t/10)+1) = W*e2;
            wp3(:,(t/10)+1) = W*e3;
            
%             q1(1,(t/10)+1) = sum(W*e1).^2;
%             q1(2,(t/10)+1) = sum(W*e2).^2;
%             q1(3,(t/10)+1) = sum(W*e3).^2; 
        end
    end
end

Inp.endT = t;
maps = W*e1;%W*v(sap:,ind(end));
salt =  W*e2;%W*v(:,ind(end-1));
neither = W*e3;
Inp.x = -L +(0:(nV1-1))*spacing;

m.Q = [mean(wp1.^2); mean(diff(wp1).^2)./mean(wp1.^2) ; mean(wp1)]; % the first row is the squared mean, the second is the nearest neighbor distance, the third is just the mean
s.Q = [mean(wp2.^2); mean(diff(wp2).^2)./mean(wp2.^2); mean(wp2)];
n.Q = [mean(wp3.^2); mean(diff(wp3).^2)./mean(wp3.^2); mean(wp3)];

wp(:,:,1) = wp1;
wp(:,:,2) = wp2;
wp(:,:,3) = wp3;

% H = MapsPlots(m, s, n, wp, growth, Inp,3);

if nargin > 2
    figname = num2str(ii);
    if ARBOR ~= 0
        figname = strcat('A', figname);
    end
    if NORM ~= 0
        figname = strcat('Norm', figname);
    end
    if eps ~=0
        figname = strcat('Eps',figname);
    end
    
    figname = strcat('EqLambda',figname);
    
%     savefig(H, figname)
    save(figname, 'm', 's', 'n', 'growth', 'lTrue', 'wp', 'Inp','W', '-v7.3')
end 
